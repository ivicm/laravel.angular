@extends('app')

@section('css')
<style type="text/css">
.form-inline .form-group input {
	width: 500px;
}
</style>
@endsection

@section('content')
<div class="container">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Catálogos</a></li>
  		<li class="active">Reportes</li>
	</ol>
	<h1 class="page-header">Reportes</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			
				
				
					
						<br />
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Reporte</th>
									<th>Acciones</th>
								</tr>
							</thead>
							
							@if($reportes->count() > 0)
								
								@foreach($reportes as $r)
								
								<tr>
									<td>{{$r->id_reporte}}</td>
									<td>{{$r->reporte}}</td>
									<td><a href="reportes/{{$r->id_reporte}}">Ver</a></td>
								</tr>
								@endforeach
							@endif
						</table>
					
					
					
					<br />
						<div class="panel panel-primary">
							<div class="panel-heading">
			    				<h3 class="panel-title">Nuevo Reporte</h3>
			  				</div>
			  				<div class="panel-body">
			    				<form class="form-inline" method="post">
								  <div class="form-group">
								    
								    
								      <input type="text" class="form-control" id="reporte" placeholder="Reporte" name="reporte">
								    
								  </div>
								 
								      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
								      <button type="submit" class="btn btn-primary">Agregar</button>
								    
								</form>
			  				</div>
						</div>
					
				
			
		</div>
	</div>
</div>
@endsection