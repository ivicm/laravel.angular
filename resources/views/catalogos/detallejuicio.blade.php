@extends('app')

@section('css')
<style type="text/css">
.form-inline .form-group input {
	width: 500px;
}
</style>
@endsection


@section('content')
<div class="container" ng-app="rubrosApp" ng-controller="RubrosController">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Catálogos</a></li>
  		<li><a href="{{ URL::to('catalogos/juicios') }}">Juicios</a></li>
  		<li class="active">Detalle</li>
	</ol>

	<h1 class="page-header">Juicios {{$juicio->juicio}}</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<ul class="list-group">
				<li class="list-group-item">
					<label class="col-md-1">ID:</label>
					<b>{{$juicio->id_juicio}}</b>
				</li>
				<li class="list-group-item">
					<label class="col-md-1">Juicio:</label>
					<b>{{$juicio->juicio}}</b>
				</li>
				<li class="list-group-item">
					<label class="col-md-1">Tipo:</label>
					<b>{{$juicio->tipo}}</b>
				</li>
			</ul>

			<div class="panel panel-default">
  				<div class="panel-heading">
    				<h3 class="panel-title">Rubros del juicio</h3>
  				</div>
  				<div class="panel-body">
  					
  					<div class="alert |% alert.type %| alert-dismissible" role="alert" ng-show="alert.show">
  						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  						|% alert.text %|
  					</div>

	    			<table class="table table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>RubroJuicio</th>
							</tr>
						</thead>
					
						<tr ng-repeat="rubro in rubros">
							<th>|% rubro.id_rubroJuicio %|</th>
							<th>|% rubro.rubro %|</th>
						</tr>	
					</table>
					<form class="form-inline">
				  		<div class="form-group" >
				    		<label for="rubro" >Rubro:</label>
				    		<input type="text" class="form-control" id="rubro" placeholder="Rubro" ng-model="inputRubro" maxlength="119">
				  		</div>
				  		<button class="btn btn-primary" type="button" ng-click="addRubro()">Agregar</button>
					</form>
					
  				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
		angular.module('rubrosApp', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('|%');
        $interpolateProvider.endSymbol('%|');
    })
  .factory('RubrosService', function($http){
  	return {
  		save : function(data){
  			return $http.post('',data);
  		}
  	}
  })
  .controller('RubrosController', function($scope, RubrosService) {
   
    $scope.rubros = {!! $juicio->rubroJuicios !!};
    $scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 	
 	$scope.addRubro = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		var rubroData = { rubro: $scope.inputRubro };
 		RubrosService.save(rubroData)
 		.success(function(data){
 			$scope.rubros.push(data);
 			$scope.inputRubro = "";
 			$scope.alert = {'show' : true, 'text' : 'Se añadio el rubro ' + data.rubro , 'type' : 'alert-success'};
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});
 		
 	};
    /*todoList.addTodo = function() {
      todoList.todos.push({text:todoList.todoText, done:false});
      todoList.todoText = '';
    };*/
 
    
 
    /*todoList.archive = function() {
      var oldTodos = todoList.todos;
      todoList.todos = [];
      angular.forEach(oldTodos, function(todo) {
        if (!todo.done) todoList.todos.push(todo);
      });
    };*/
  });

	/*$("#btnAgregar").click(function(){
		$.ajax({
			url: '/detalleJuicio',
			type: post
		});
	});*/
</script>
@endsection