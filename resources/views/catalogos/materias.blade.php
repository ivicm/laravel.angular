@extends('app')

@section('css')
<style type="text/css">
.form-inline .form-group input {
	width: 500px;
}
</style>
@endsection

@section('content')
<div class="container">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Catálogos</a></li>
  		<li class="active">Materias</li>
	</ol>
	<h1 class="page-header">Materias</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			
				
				
					
						<br />
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Materia</th>
									<th>Acciones</th>
								</tr>
							</thead>
							
							@if($materias->count() > 0)
								
								@foreach($materias as $m)
								
								<tr>
									<td>{{$m->id_materia}}</td>
									<td>{{$m->materia}}</td>
									<td></td>
								</tr>
								@endforeach
							@endif
						</table>
					
					
					
					<br />
						<div class="panel panel-primary">
							<div class="panel-heading">
			    				<h3 class="panel-title">Nueva Materia</h3>
			  				</div>
			  				<div class="panel-body">
			    				<form class="form-inline" method="post">
								  <div class="form-group">
								    
								    
								      <input type="text" class="form-control" id="materia" placeholder="Materia" name="materia">
								    
								  </div>
								 
								      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
								      <button type="submit" class="btn btn-primary">Agregar</button>
								    
								</form>
			  				</div>
						</div>
					
				
			
		</div>
	</div>
</div>
@endsection