@extends('app')

@section('content')
<div class="container">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Catálogos</a></li>
  		<li><a href="{{ URL::to('catalogos/tipojuzgados') }}">Tipos Juzgado</a></li>
  		<li class="active">Detalle</li>
	</ol>

	<h1 class="page-header">Juzgado {{$tipojuzgado->tipo}}</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<form class="form-horizontal" method="post">
			  <div class="form-group">
    			<label class="col-sm-2 control-label">ID</label>
    			<div class="col-sm-10">
      				<p class="form-control-static">{{ $tipojuzgado->id_tipojuzgado }}</p>
    			</div>
  			  </div>
			  <div class="form-group">
			    <label for="tipo" class="col-sm-2 control-label">Tipo</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="tipo" placeholder="Tipo de juzgado" name="tipo" value="{{ $tipojuzgado->tipo }}">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{ $tipojuzgado->descripcion }}">
			    </div>
			  </div>
			 
			  <div class="form-group">
				<label class="col-sm-2 control-label">Materias:</label>
				<div class="col-sm-10">
					@foreach($checks as $c)

					<div class="checkbox">
							<label>
									<input type="checkbox" name="materias[]" value="{{ $c->value }}" {{ $c->checked }}>
								
							
								{{ $c->label }}
							</label>
					</div>
					@endforeach
				</div>
			  </div>
			 
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			      <button type="submit" class="btn btn-primary">Agregar</button>
			    </div>
			  </div>
			</form>

		</div>
	</div>
</div>
@endsection