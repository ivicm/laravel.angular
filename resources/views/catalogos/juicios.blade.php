@extends('app')

@section('content')
<div class="container" ng-app="juiciosApp" ng-controller="JuiciosController">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Catálogos</a></li>
  		<li class="active">Juicios</li>
	</ol>
	<h1 class="page-header">Juicios</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div role="tabpane">
				<ul class="nav nav-tabs" role="tablist">
    				<li role="presentation" class="active"><a href="#lista" aria-controls="lista" role="tab" data-toggle="tab">Lista</a></li>
    				<li role="presentation"><a href="#nuevo" aria-controls="nuevo" role="tab" data-toggle="tab">Nuevo</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="lista">
						<br />
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Juicio</th>
									<th>Materia</th>
									<th>Acciones</th>
								</tr>
							</thead>
							@foreach ($juicios as $j)
								<tr>
									<td>{{$j->id_juicio}}</td>
									<td>{{$j->juicio}}</td>
									<td>{{$j->materia->materia}}</td>
									<td>
										<a href="juicios/{{$j->id_juicio}}">Ver</a> |
										<a href="#" ng-click="findJuicio({{$j->id_juicio}})">Editar</a>
									</td>
								</tr>
							@endforeach
						</table>
					</div>
					<div role="tabpanel" class="tab-pane" id="nuevo">
					<br />
						<div class="panel panel-primary">
							<div class="panel-heading">
			    				<h3 class="panel-title">Nuevo Juicio</h3>
			  				</div>
			  				<div class="panel-body">
			    				<form class="form-horizontal" method="post">
								  <div class="form-group">
								    <label for="juicio" class="col-sm-2 control-label">Juicio</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="juicio" placeholder="Juicio" name="juicio">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="tipo" class="col-sm-2 control-label">Tipo</label>
								    <div class="col-sm-10">
								      <select class="form-control" id="tipo" name="tipo">
								      	@foreach ($materias as $m)
			  							<option value="{{ $m->id_materia }}">{{ $m->materia }}</option>
			  							@endforeach
			  						  </select>
								    </div>
								  </div>
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
								      <button type="submit" class="btn btn-primary">Agregar</button>
								    </div>
								  </div>
								</form>
			  				</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="modalJuicio">
			  <div class="modal-dialog">
			    <div class="modal-content">
			    	<form class="form-horizontal" method="post" action="">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title">Juicio</h4>
			      </div>
			      <div class="modal-body">
			       	
					  <div class="form-group">
					    <label for="juicio" class="col-sm-2 control-label">Juicio</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="juicio" placeholder="Juicio" name="juicio" ng-model="data.juicio">
					      <input type="hidden" id="id" name="id" ng-model="data.id" ng-value="data.id" />
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="tipo" class="col-sm-2 control-label">Tipo</label>
					    <div class="col-sm-10">
					      <select class="form-control" id="tipo" name="tipo" ng-model="data.id_materia">
					      	@foreach ($materias as $m)
  							<option value="{{ $m->id_materia }}">{{ $m->materia }}</option>
  							@endforeach
  						  </select>
					    </div>
					  </div>
			      </div>
			      <div class="modal-footer">
			      	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			        <button type="submit" class="btn btn-primary">Guardar</button>
			      </div>
			    </div><!-- /.modal-content -->
			    </form>
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->

		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
		angular.module('juiciosApp', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('|%');
        $interpolateProvider.endSymbol('%|');
    })
	.controller('JuiciosController', function($scope, $filter) {
   
    $scope.juicios = {!! $juicios !!};
    $scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
    $scope.data = {'id':0, 'juicio':'', 'id_materia':0}
 	
    $scope.findJuicio = function(id){
    	$("#modalJuicio").modal();
    	var obj = $filter('filter')($scope.juicios, function(d){ return d.id_juicio === parseInt(id)})[0];
    	$scope.data = {'id': obj.id_juicio, 'juicio': obj.juicio, 'id_materia': obj.materia.id_materia};
    };

 	/*
 	$scope.addRubro = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		var rubroData = { rubro: $scope.inputRubro };
 		RubrosService.save(rubroData)
 		.success(function(data){
 			$scope.rubros.push(data);
 			$scope.inputRubro = "";
 			$scope.alert = {'show' : true, 'text' : 'Se añadio el rubro ' + data.rubro , 'type' : 'alert-success'};
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});
 		
 	};
 	*/
    /*todoList.addTodo = function() {
      todoList.todos.push({text:todoList.todoText, done:false});
      todoList.todoText = '';
    };*/
 
    
 
    /*todoList.archive = function() {
      var oldTodos = todoList.todos;
      todoList.todos = [];
      angular.forEach(oldTodos, function(todo) {
        if (!todo.done) todoList.todos.push(todo);
      });
    };*/
  });

	/*$("#btnAgregar").click(function(){
		$.ajax({
			url: '/detalleJuicio',
			type: post
		});
	});*/
</script>
@endsection