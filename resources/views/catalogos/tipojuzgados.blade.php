@extends('app')

@section('content')
<div class="container">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Catálogos</a></li>
  		<li class="active">Tipos de Juzgado</li>
	</ol>
	<h1 class="page-header">Tipos de Juzgado</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			
				
				
					
						<br />
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Tipo</th>
									<th>Descripción</th>
									<th>Materias</th>
									<th>Acciones</th>
								</tr>
							</thead>
							
							@if($tipos->count() > 0)
								
								@foreach($tipos as $t)
								
								<tr>
									<td>{{$t->id_tipojuzgado}}</td>
									<td>{{$t->tipo}}</td>
									<td>{{$t->descripcion}}</td>
									<td>
										<ul class="list-unstyled">
											@foreach($t->materias as $m)
											<li>{{$m->materia}}</li>
											@endforeach		
										</ul>
									</td>
									<td><a href="tipojuzgados/{{$t->id_tipojuzgado}}">Editar</a></td>
								</tr>
								@endforeach
							@endif
						</table>
					
					
					
					<br />
						<div class="panel panel-primary">
							<div class="panel-heading">
			    				<h3 class="panel-title">Nuevo Tipo</h3>
			  				</div>
			  				<div class="panel-body">
			  					<form class="form-horizontal" method="post">
								  <div class="form-group">
								    <label for="tipo" class="col-sm-2 control-label">Tipo</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="tipo" placeholder="Tipo de juzgado" name="tipo">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion">
								    </div>
								  </div>
								 
								  <div class="form-group">
									<label class="col-sm-2 control-label">Materias:</label>
									<div class="col-sm-10">
										@foreach($materias as $mat)
										<div class="checkbox">
		  									<label>
		    									<input type="checkbox" name="materias[]" value="{{ $mat->id_materia }}">
		    										{{ $mat->materia }}
		  									</label>
										</div>
										@endforeach
									</div>
								  </div>
								 
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
								      <button type="submit" class="btn btn-primary">Agregar</button>
								    </div>
								  </div>
								</form>
							</div>
						</div>
					
				
			
		</div>
	</div>
</div>
@endsection