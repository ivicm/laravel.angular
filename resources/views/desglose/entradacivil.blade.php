@extends('app')

@section('content')
<div class="container" ng-app="DesgloseEntradaCivilApp" ng-controller="DesgloseEntradaCivilController">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Desglose</a></li>
  		<li><a href="#">Civil</a></li>
  		<li class="active"><a href="#">Entradas</a></li>
	</ol>
	<h1 class="page-header">Desglose Entrada Civil</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			
			<div class="alert |% alert.type %| alert-dismissible" role="alert" ng-show="alert.show">
  						|% alert.text %|
  			</div>			
			<form class="form-horizontal" method="post">
			  <div class="form-group">
			    <label for="juzgado" class="col-sm-2 control-label">Juzgado:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juzgado" ng-model="juzgado" ng-change="getJuicios()">
			      	@foreach ($juzgados as $jz)
			      		<option value="{{ $jz->id_juzgado }}">{{ $jz->prefijo }}</option>
			      	@endforeach
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="anyo" class="col-sm-2 control-label">Año:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="anyo" ng-model="anyo">
			      	@for($i = date("Y"); $i > 2011; $i--)
			      		<option value="{{ $i }}"> {{ $i }} </option>
			      	@endfor
			      </select>
			    </div>
			  </div>
			  
			  <div class="form-group">
			    <label for="juicio" class="col-sm-2 control-label">Juicio:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juicio" ng-model="juicio" ng-change="getRubros()">
			      		<option value="|% j.id_juicio %|" ng-repeat="j in juicios">|% j.juicio %| (|% j.materia.materia %|)</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="rubro" class="col-sm-2 control-label">Rubro:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="rubro" ng-model="rubro">
			      		<option value="|% rubro.id_rubroJuicio %|" ng-repeat="rubro in rubros" >|% rubro.rubro %|</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="button" class="btn btn-primary" ng-click="getData()">Mostrar</button>
			    </div>
			  </div>
			</form>
			<table class="table table-striped" ng-show="showTable">
				<thead>
					<tr>
						<th>Mes</th>
						<th>Ingreso</th>
						<th>Reingreso</th>
						<th>Archivo Provisional</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="row in tabla">
						<td>|% getMes(row.mes) %|</td>
						<td>|% row.ingreso %|</td>
						<td>|% row.reingreso %|</td>
						<td>|% row.archivoprovisional %|</td>
						<td><a href="#" ng-click="findEntrada($index)">Editar</a></td>
					</tr>
				</tbody>
			</table>
			<div>

			<div class="modal fade" id="modalDesglose">
			  <div class="modal-dialog">
			    <div class="modal-content">
			    	<form class="form-horizontal" method="post" action="">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title">Entradas</h4>
			      </div>
			      <div class="modal-body">
			       	
					  <div class="form-group">
					    <label for="juicio" class="col-sm-2 control-label">Ingresos:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="ingreso" name="ingreso" ng-model="data.ingreso">
					      <input type="hidden" id="id" name="id" ng-model="data.id" ng-value="data.id" />
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="juicio" class="col-sm-2 control-label">Reingresos:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="reingreso" name="reingreso" ng-model="data.reingreso">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="juicio" class="col-sm-2 control-label">Archivo provisional:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="ap" name="ap" ng-model="data.ap">
					    </div>
					  </div>
			      </div>
			      <div class="modal-footer">
			      	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			        <button type="button" class="btn btn-primary" ng-click="updateData()">Guardar</button>
			      </div>
			    </div><!-- /.modal-content -->
			    </form>
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->

		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
		angular.module('DesgloseEntradaCivilApp', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('|%');
        $interpolateProvider.endSymbol('%|');
    })
  .factory('DesgloseEntradaCivilService', function($http){
  	return {
  		getRubrosByJuicio : function(data){
  			return $http.get('{{ URL::to("catalogos/juicios") }}/'+ data + '/rubros');
  		},
  		getDesgloseEntradaCivil : function(data){
  			//return $http.get('detalle?juzgado='+data.juzgado+'&anyo='+data.anyo+'&rubro='+data.rubro);
  			return $http.get('entrada/detalle',{ params : data });
  		},
  		getJuiciosByTipoJuzgado : function(data) {
  			return $http.get('{{ URL::to("juzgados") }}/'+ data + '/juicios');
  		},
  		updateEntradaCivil : function(data) {
  			return $http.post('entrada/detalle', data);
  		}
  	}
  })
  .controller('DesgloseEntradaCivilController', function($scope, $interval, DesgloseEntradaCivilService) {
    var fecha = new Date();
    $scope.rubros = {!! $rubros !!};
    $scope.juicios = {!! $juicios !!};
    $scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
    $scope.juicio = {{ $juicios->first()->id_juicio }};
    $scope.anyo = fecha.getFullYear();
    $scope.juzgado = {{ $juzgados->first()->id_juzgado }};
    $scope.rubro  =  $scope.rubros[0].id_rubroJuicio;
    $scope.showTable = false;
    $scope.tabla = [];
    $scope.data = {'id': 0, 'ingreso': 0, 'reingreso': 0, 'ap': 0};
     	
 	$scope.getRubros = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		DesgloseEntradaCivilService.getRubrosByJuicio($scope.juicio)
 		.success(function(data){
 			$scope.rubros = data;
 			$scope.rubro = $scope.rubros[0].id_rubroJuicio;
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

 	$scope.getData = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		var data = {'juzgado': $scope.juzgado, 'anyo' : $scope.anyo, 'rubro' :  $scope.rubro };
 		DesgloseEntradaCivilService.getDesgloseEntradaCivil(data)
 		.success(function(response){
 			$scope.tabla = response;
 			$scope.showTable = true;
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 			$scope.tabla = false;
 		});

 	};

 	$scope.getJuicios = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		DesgloseEntradaCivilService.getJuiciosByTipoJuzgado($scope.juzgado)
 		.success(function(data){
 			$scope.juicios = data;
 			$scope.juicio = $scope.juicios[0].id_juicio;
 			$scope.getRubros();
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

 	$scope.getMes = function(mes){
 		var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
 		"Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

 		return meses[mes-1];
 	}

 	$scope.findEntrada = function(index){
    	var obj = $scope.tabla[index];
    	$scope.data = {'id': obj.id_entradas_civil
    	               , 'ingreso': obj.ingreso
    	               , 'reingreso': obj.reingreso
    	               , 'ap': obj.archivoprovisional };
    	$("#modalDesglose").modal();
    };

    $scope.updateData = function() {
    	$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		DesgloseEntradaCivilService.updateEntradaCivil($scope.data)
 		.success(function(response){
 			if(response.status == "OK")
 			{
 			    $scope.getData();
 			    $scope.alert = {'show' : true, 'text' : 'La información ha sido guardada.' + response.mensaje + ' ' + JSON.stringify($scope.data), 'type' : 'alert-success'};
 			}
 			else
 			{
 				$scope.alert = {'show' : true, 'text' : response.mensaje , 'type' : 'alert-danger'};	
 			}	
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
    	$("#modalDesglose").modal("hide");	
    };

  });
</script>
@endsection