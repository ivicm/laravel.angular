@extends('app')

@section('content')
<div class="container" ng-app="DesgloseSalidaFamiliarApp" ng-controller="DesgloseSalidaFamiliarController">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Desglose</a></li>
  		<li><a href="#">Familiar</a></li>
  		<li class="active"><a href="#">Salidas</a></li>
	</ol>
	<h1 class="page-header">Desglose Salida Familiar</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			
			<div class="alert |% alert.type %| alert-dismissible" role="alert" ng-show="alert.show">
  						|% alert.text %|
  			</div>			
			<form class="form-horizontal" method="post">
			  <div class="form-group">
			    <label for="juzgado" class="col-sm-2 control-label">Juzgado:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juzgado" ng-model="juzgado" ng-change="getJuicios()">
			      	@foreach ($juzgados as $jz)
			      		<option value="{{ $jz->id_juzgado }}">{{ $jz->prefijo }}</option>
			      	@endforeach
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="anyo" class="col-sm-2 control-label">Año:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="anyo" ng-model="anyo">
			      	@for($i = date("Y"); $i > 2011; $i--)
			      		<option value="{{ $i }}"> {{ $i }} </option>
			      	@endfor
			      </select>
			    </div>
			  </div>
			  
			  <div class="form-group">
			    <label for="juicio" class="col-sm-2 control-label">Juicio:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juicio" ng-model="juicio" ng-change="getRubros()">
			      		<option value="|% j.id_juicio %|" ng-repeat="j in juicios">|% j.juicio %| (|% j.materia.materia %|)</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="rubro" class="col-sm-2 control-label">Rubro:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="rubro" ng-model="rubro">
			      		<option value="|% rubro.id_rubroJuicio %|" ng-repeat="rubro in rubros" >|% rubro.rubro %|</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="button" class="btn btn-primary" ng-click="getData()">Mostrar</button>
			    </div>
			  </div>
			</form>
			<table class="table table-striped" ng-show="showTable">
				<thead>
					<tr>
						<th>Mes</th>
						<th>Sentencia Procedente</th>
						<th>Sentencia Improcedente</th>
						<th>Auto</th>
						<th>Excusa - Recusación</th>
						<th>Competencia</th>
						<th>Acumulación</th>
						<th>Archivo Provisional</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="row in tabla">
						<td>|% getMes(row.mes) %|</td>
						<td>|% row.sprocedente %|</td>
						<td>|% row.simprocedente %|</td>
						<td>|% row.auto %|</td>
						<td>|% row.excusarecusacion %|</td>
						<td>|% row.competencia %|</td>
						<td>|% row.acumulacion %|</td>
						<td>|% row.archivoprovisional %|</td>
						<td><a href="#" ng-click="findSalida($index)">Editar</a></td>
					</tr>
				</tbody>
			</table>
			<div>

			<div class="modal fade" id="modalDesglose">
			  <div class="modal-dialog">
			    <div class="modal-content">
			    	<form class="form-horizontal" method="post" action="">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title">Salidas</h4>
			      </div>
			      <div class="modal-body">
			       	
					  <div class="form-group">
					    <label for="sprocedente" class="col-sm-2 control-label">Sentencia procedente:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="sprocedente" name="sprocedente" ng-model="data.sprocedente">
					      <input type="hidden" id="id" name="id" ng-model="data.id" ng-value="data.id" />
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="simprocedente" class="col-sm-2 control-label">Sentencia improcedente:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="simprocedente" name="simprocedente" ng-model="data.simprocedente">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="auto" class="col-sm-2 control-label">Auto:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="auto" name="auto" ng-model="data.auto">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="excusarecusacion" class="col-sm-2 control-label">Excusa - Recusación:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="excusarecusacion" name="excusarecusacion" ng-model="data.excusarecusacion">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="competencia" class="col-sm-2 control-label">Competencia:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="competencia" name="competencia" ng-model="data.competencia">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="acumulacion" class="col-sm-2 control-label">Acumulación:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="acumulacion" name="acumulacion" ng-model="data.acumulacion">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="ap" class="col-sm-2 control-label">Archivo provisional:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="ap" name="ap" ng-model="data.ap">
					    </div>
					  </div>
			      </div>
			      <div class="modal-footer">
			      	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			        <button type="button" class="btn btn-primary" ng-click="updateData()">Guardar</button>
			      </div>
			    </div><!-- /.modal-content -->
			    </form>
			  </div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
		angular.module('DesgloseSalidaFamiliarApp', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('|%');
        $interpolateProvider.endSymbol('%|');
    })
  .factory('DesgloseSalidaFamiliarService', function($http){
  	return {
  		getRubrosByJuicio : function(data){
  			return $http.get('{{ URL::to("catalogos/juicios") }}/'+ data + '/rubros');
  		},
  		getDesgloseSalidaFamiliar : function(data){
  			//return $http.get('detalle?juzgado='+data.juzgado+'&anyo='+data.anyo+'&rubro='+data.rubro);
  			return $http.get('salida/detalle',{ params : data });
  		},
  		getJuiciosByTipoJuzgado : function(data) {
  			return $http.get('{{ URL::to("juzgados") }}/'+ data + '/juicios');
  		},
  		updateSalidaFamiliar : function(data) {
  			return $http.post('salida/detalle', data);
  		}
  	}
  })
  .controller('DesgloseSalidaFamiliarController', function($scope, $interval, DesgloseSalidaFamiliarService) {
    var fecha = new Date();
    $scope.rubros = {!! $rubros !!};
    $scope.juicios = {!! $juicios !!};
    $scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
    $scope.juicio = {{ $juicios->first()->id_juicio }};
    $scope.anyo = fecha.getFullYear();
    $scope.juzgado = {{ $juzgados->first()->id_juzgado }};
    $scope.rubro  =  $scope.rubros[0].id_rubroJuicio;
    $scope.showTable = false;
    $scope.tabla = [];
     	
 	$scope.getRubros = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		DesgloseSalidaFamiliarService.getRubrosByJuicio($scope.juicio)
 		.success(function(data){
 			$scope.rubros = data;
 			$scope.rubro = $scope.rubros[0].id_rubroJuicio;
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

 	$scope.getData = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		var data = {'juzgado': $scope.juzgado, 'anyo' : $scope.anyo, 'rubro' :  $scope.rubro };
 		DesgloseSalidaFamiliarService.getDesgloseSalidaFamiliar(data)
 		.success(function(response){
 			$scope.tabla = response;
 			$scope.showTable = true;
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 			$scope.tabla = false;
 		});

 	};

 	$scope.getJuicios = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		DesgloseSalidaFamiliarService.getJuiciosByTipoJuzgado($scope.juzgado)
 		.success(function(data){
 			$scope.juicios = data;
 			$scope.juicio = $scope.juicios[0].id_juicio;
 			$scope.getRubros();
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

 	$scope.getMes = function(mes){
 		var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
 		"Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

 		return meses[mes-1];
 	}

 	$scope.findSalida = function(index){
    	var obj = $scope.tabla[index];
    	$scope.data = {'id': obj.id_salidas_familiar
    	               , 'sprocedente': obj.sprocedente
    	               , 'simprocedente': obj.simprocedente
    	               , 'auto' : obj.auto
    	               , 'excusarecusacion' : obj.excusarecusacion
    	               , 'competencia' : obj.competencia
    	               , 'acumulacion' : obj.acumulacion
    	               , 'ap': obj.archivoprovisional };
    	$("#modalDesglose").modal();
    };

    $scope.updateData = function() {
    	$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		DesgloseSalidaFamiliarService.updateSalidaFamiliar($scope.data)
 		.success(function(response){
 			if(response.status == "OK")
 			{
 			    $scope.getData();
 			    $scope.alert = {'show' : true, 'text' : 'La información ha sido guardada.' + response.mensaje + ' ' + JSON.stringify($scope.data), 'type' : 'alert-success'};
 			}
 			else
 			{
 				$scope.alert = {'show' : true, 'text' : response.mensaje , 'type' : 'alert-danger'};	
 			}	
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
    	$("#modalDesglose").modal("hide");	
    };
  });
</script>
@endsection