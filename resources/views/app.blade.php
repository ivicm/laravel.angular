<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Estadística :: Consejo del Poder Judicial del Estado de Michoacán</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- Custom css -->
	@yield('css')	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Estadística</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/') }}">Home</a></li>
					<li><a href="{{ url('/juzgados') }}">Juzgados</a></li>
					<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Catálogos <span class="caret"></span></a>
          				<ul class="dropdown-menu" role="menu">
            				<li><a href="{{ url('/catalogos/juicios') }}">Juicios</a></li>
            				<li class="divider"></li>
            				<li><a href="{{ url('/catalogos/materias') }}">Materia</a></li>
            				<li><a href="{{ url('/catalogos/tipojuzgados') }}">Tipos de Juzgado</a></li>
          				</ul>
        			</li>
        			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Datos<span class="caret"></span></a>
          				<ul class="dropdown-menu" role="menu">
            				<li><a href="{{ url('/captura/civil/entrada') }}">Entrada Civil</a></li>
            				<li><a href="{{ url('/captura/civil/salida') }}">Salida Civil</a></li>
            				<li class="divider"></li>
            				<li><a href="{{ url('/desglose/civil/entrada') }}">Desglose Entrada Civil</a></li>
            				<li><a href="{{ url('/desglose/civil/salida') }}">Desglose Salida Civil</a></li>
            				<li class="divider"></li>
            				<li><a href="{{ url('/captura/familiar/entrada') }}">Entrada Familiar</a></li>
            				<li><a href="{{ url('/captura/familiar/salida') }}">Salida Familiar</a></li>
            				<li class="divider"></li>
            				<li><a href="{{ url('/desglose/familiar/entrada') }}">Desglose Entrada Familiar</a></li>
            				<li><a href="{{ url('/desglose/familiar/salida') }}">Desglose Salida Familiar</a></li>
          				</ul>
        			</li>
        			<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Informe<span class="caret"></span></a>
          				<ul class="dropdown-menu" role="menu">
            				<li><a href="{{ url('/informe/civil') }}">Graficas Civil</a></li>
            				<li><a href="{{ url('/reporte/civil') }}">Reporte Civil</a></li>
            				<li class="divider"></li>
            				<li><a href="{{ url('/informe/familiar') }}">Graficas Familiar</a></li>
            				<li><a href="{{ url('/reporte/familiar') }}">Reporte Familiar</a></li>
          				</ul>
          				
        			</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{URL::asset('js/angular.js')}}"></script>
	@yield('js')

</body>
</html>
