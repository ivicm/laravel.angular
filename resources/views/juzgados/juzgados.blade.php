@extends('app')

@section('content')
<div class="container">
	<h1 class="page-header">Lista de Juzgados</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Prefijo</th>
						<th>Tipo</th>
					</tr>
				</thead>
				@foreach ($juzgados as $j)
					<tr>
						<th>{{$j->id_juzgado}}</th>
						<th>{{$j->prefijo}}</th>
						<th>{{$j->tipojuzgado->tipo}}</th>
					</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
@endsection