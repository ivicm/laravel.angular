@extends('app')

@section('content')
<div class="container">
	<h1 class="page-header">Detalle de Juzgado</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<ul class="list-group">
				<li class="list-group-item">
					<label class="col-md-1">ID:</label>
					<b>{{$juzgado->id_juzgado}}</b>
				</li>
				<li class="list-group-item">
					<label class="col-md-1">Prefijo:</label>
					<b>{{$juzgado->prefijo}}</b>
				</li>
				<li class="list-group-item">
					<label class="col-md-1">Tipo:</label>
					<b>{{$juzgado->tipojuzgado->tipo}}</b>
				</li>
			</ul>
		</div>
	</div>
</div>
@endsection