@extends('app')

@section('css')
<style type="text/css">
div.with-scroll {
  overflow: auto;
}
</style>
@endsection

@section('content')
<div class="container" ng-app="ReporteCivilApp" ng-controller="ReporteCivilController">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Informes</a></li>
  		<li><a href="#">Reporte</a></li>
  		<li class="active"><a href="#">Civil</a></li>
	</ol>
	<h1 class="page-header">Reporte Civil</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			
			<div class="alert |% alert.type %| alert-dismissible" role="alert" ng-show="alert.show">
  						|% alert.text %|
  			</div>			
			<form class="form-horizontal" method="post">
			  <div class="form-group">
			    <label for="juzgado" class="col-sm-2 control-label">Juzgado:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juzgado" ng-model="juzgado" ng-change="getJuicios()">
			      	@foreach ($juzgados as $jz)
			      		<option value="{{ $jz->id_juzgado }}">{{ $jz->prefijo }}</option>
			      	@endforeach
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="anyo" class="col-sm-2 control-label">Año:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="anyo" ng-model="anyo">
			      	@for($i = date("Y"); $i > 2011; $i--)
			      		<option value="{{ $i }}"> {{ $i }} </option>
			      	@endfor
			      </select>
			    </div>
			  </div>
			  
			  <div class="form-group">
			    <label for="juicio" class="col-sm-2 control-label">Juicio:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juicio" ng-model="juicio" ng-change="getRubros()">
			      		<option value="|% j.id_juicio %|" ng-repeat="j in juicios">|% j.juicio %| (|% j.materia.materia %|)</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="rubro" class="col-sm-2 control-label">Rubro:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="rubro" ng-model="rubro">
			      		<option value="|% rubro.id_rubroJuicio %|" ng-repeat="rubro in rubros" >|% rubro.rubro %|</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			  	<div class="col-sm-offset-2 col-sm-10">
			      <div class="checkbox">
    			    <label class="control-label">
      				  <input type="checkbox" ng-model="pormes"> Desglose por mes
    			    </label>
  			      </div>
  			    </div>
  			 </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="button" class="btn btn-primary" ng-click="getTabla()">Mostrar</button>
			    </div>
			  </div>
			</form>

			<div id="tablas"></div>

		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
		angular.module('ReporteCivilApp', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('|%');
        $interpolateProvider.endSymbol('%|');
    })
  .factory('ReporteCivilService', function($http){
  	return {
  		getRubrosByJuicio : function(data){
  			return $http.get('{{ URL::to("catalogos/juicios") }}/'+ data + '/rubros');
  		},
  		getJuiciosByTipoJuzgado : function(data) {
  			return $http.get('{{ URL::to("juzgados") }}/'+ data + '/juicios');
  		}
  	}
  })
  .controller('ReporteCivilController', function($scope, $interval, ReporteCivilService, $compile) {
    var fecha = new Date();
    $scope.rubros = {!! $rubros !!};
    $scope.juicios = {!! $juicios !!};
    $scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
    $scope.juicio = {{ $juicios->first()->id_juicio }};
    $scope.anyo = fecha.getFullYear();
    $scope.juzgado = {{ $juzgados->first()->id_juzgado }};
    $scope.rubros.splice(0,0,{"id_rubroJuicio":0,"id_juicio":$scope.juicio,"rubro":"Todos"});
    $scope.rubro  =  $scope.rubros[0].id_rubroJuicio;
    $scope.showTable = false;
    $scope.tabla = [];
     	
 	$scope.getRubros = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		ReporteCivilService.getRubrosByJuicio($scope.juicio)
 		.success(function(data){
 			$scope.rubros = data;
      $scope.rubros.splice(0,0,{"id_rubroJuicio":0,"id_juicio":$scope.juicio,"rubro":"Todos"});
 			$scope.rubro = $scope.rubros[0].id_rubroJuicio;
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

 	$scope.getTabla = function(){
      var desglosado = 0;
      if($scope.pormes == 1){
        desglosado = 1;
      }
 			var options = '{ "juzgado" :'  + $scope.juzgado  
 			             + ', "anyo" :'  + $scope.anyo
 			             + ', "juicio" : ' + $scope.juicio
 			             + ', "rubro" : ' + $scope.rubro
 			             + ', "desglosado" : ' + desglosado
 			             + '}';
 			angular.element('#tablas').append($compile("<div tablareporte options='" + options + "' />")($scope));
 	};

 	$scope.getJuicios = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		ReporteCivilService.getJuiciosByTipoJuzgado($scope.juzgado)
 		.success(function(data){
 			$scope.juicios = data;
 			$scope.juicio = $scope.juicios[0].id_juicio;
 			$scope.getRubros();
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

 	$scope.getMes = function(mes){
 		var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
 		"Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

 		return meses[mes-1];
 	}
  })
  .directive('tablareporte', function(){
  	return {
  		scope : {},
  		template : "<div class='panel panel-default'>"+
                     "<div class='panel-heading'>"+
        			  "Reporte de |% titulo %|"+
                        "<button type='button' class='close' aria-label='Close' ng-click='close()'><span aria-hidden='true'>&times;</span></button>" +
                     "</div>"+
        			  "<div class='with-scroll'><table class='table table-striped'>"+
                    "<thead><tr><th></th>"+
                      "<th colspan='3'>Entradas</th>"+
                      "<th colspan='7'>Salidas</th>"+
                     "</tr>"+
                      "<tr>"+
                        "<th></th>"+
                        "<th>Ingreso</th>"+
                        "<th>Reingreso</th>"+
                        "<th>Archivo Provisional</th>"+
                        "<th>Sentencia Procedente</th>"+
                        "<th>Sentencia Improcedente</th>"+
                        "<th>Auto</th>"+
                        "<th>Excusa</th>"+
                        "<th>Recusación</th>"+
                        "<th>Acumulación</th>"+
                        "<th>Archivo Provisional</th>"+
                      "</tr>"+
                    "</thead>"+
                    "<tbody>"+
                      "<tr ng-repeat='row in rows'>"+
                        "<td>|% row['Etiqueta'] %|</td>"+ 
                        "<td>|% row['Ingreso'] %|</td>"+
                        "<td>|% row['Reingreso'] %|</td>"+
                        "<td>|% row['Archivo Provisional (E)'] %|</td>"+
                        "<td>|% row['Sentencia Procedente'] %|</td>"+
                        "<td>|% row['Sentencia Improcedente'] %|</td>"+
                        "<td>|% row['Auto'] %|</td>"+
                        "<td>|% row['Excusa'] %|</td>"+
                        "<td>|% row['Recusacion'] %|</td>"+
                        "<td>|% row['Acumulacion'] %|</td>"+
                        "<td>|% row['Archivo Provisional (S)'] %|</td>"+
                      "</tr>"+
                    "</tbody>"+
                  "</table></div>"+
  		           "</div>",
      controller : ['$scope', '$http', function($scope, $http) {
        $scope.getReporte = function(options) {
          p = JSON.parse(options);
          $http.get('civil/generar',{ params : p })
          .success(function(data) {
            $scope.titulo = data.titulo;
            $scope.rows = data.filas;
          });
        }

        $scope.close = function() {
           var panelScope;
           panelScope = $scope.$new();
           $scope.element.remove();
           panelScope.$destroy();
        }
      }],
  		link : function(scope, iElement, iAttrs) {
      		scope.getReporte(iAttrs.options);
          scope.element = iElement;
    	}
  	};
  });
</script>
@endsection