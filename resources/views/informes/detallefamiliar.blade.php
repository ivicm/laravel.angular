@extends('app')

@section('css')
<link href="{{ asset('/css/angular-chart.css') }}" rel="stylesheet">
<style type="text/css">
li.contain-select {
  padding:0.5em 1em;
}
</style>
@endsection

@section('content')
<div class="container" ng-app="InformeApp">

  <ol class="breadcrumb">
      <li><a href="{{ URL::to('') }}">Inicio</a></li>
      <li><a href="#">Informe</a></li>
      <li><a href="#">Familiar</a></li>
      <li class="active">General</li>
  </ol>
	<h1 class="page-header">Estadística <small>{{$titulo}}</small>
    <div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Juzgado: <b>{{$label_juzgado}}</b> <span class="caret"></span>
          </button>
          <ul class="dropdown-menu with-select" role="menu">
            <li class="contain-select">
              <form ng-controller="ChangeController">
              <select class="form-control" ng-model="prefijo" ng-change="cambiarJuzgado()">
                  <option value="Todos">Todos</option>
                @foreach($juzgados as $j)
                  <option value ="{{$j->prefijo}}">{{ $j->prefijo }}</option>
                @endforeach
              </select>
            </form>
            </li>
          </ul>
        </div>
        <div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Año: <b>{{$label_anyo}}</b> <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
              <li><a href="{{ url($path) }}">Todos</a></li>
              @foreach($anyos as $a)
              <li><a href="{{ url($path).'/'.$a }}">{{$a}}</a></li>
              @endforeach
          </ul>
        </div> 
  </h1>

	<div class="row-fluid">
    
		<div class="col-md-10 col-md-offset-1">
      
			<div class="panel panel-default">
  				<div class="panel-heading">
    				<h3 class="panel-title">General
            </h3>
  				</div>
  				<div class="panel-body" >
              
            <div role="tabpanel" ng-controller="GlobalController">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#g1" aria-controls="home" role="tab" data-toggle="tab" ng-click="loadG1()">General</a></li>
                <li role="presentation"><a href="#g2" aria-controls="profile" role="tab" data-toggle="tab" ng-click="loadG2()">Entradas</a></li>
                <li role="presentation"><a href="#g3" aria-controls="profile" role="tab" data-toggle="tab" ng-click="loadG3()">Salidas</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content" >
                <div role="tabpanel" class="tab-pane active" id="g1" >
                  <canvas id="chartG1" class="chart chart-line" data="data"
                                labels="labels" legend="true" series="series"
                                click="onClick"></canvas> 
                </div>
                <div role="tabpanel" class="tab-pane" id="g2">
                  <canvas id="chartG2" class="chart chart-bar" data="data2"
                                labels="labels2" legend="true" series="series2"
                                click="onClick"></canvas> 
                </div>
                <div role="tabpanel" class="tab-pane" id="g3">
                  <canvas id="chartG3" class="chart chart-bar" data="data3"
                                labels="labels3" legend="true" series="series3"
                                click="onClick"></canvas> 
                </div>
              </div>
            </div>
  				</div>
			</div>

      <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Por Juicio
            </h3>
          </div>
          <div class="panel-body" >
              
            <div role="tabpanel" ng-controller="JuicioController">
              <p>
                <form>
                  <div class="form-group">
                    <select class="form-control" ng-model="juicio" ng-change="getRubros();">
                      @foreach($juicios as $j)
                      <option value="{{ $j->id_juicio }}">{{ $j->juicio }} {{ $j->materia->materia }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                      <select class="form-control" id="rubro" ng-model="rubro" ng-change="getCharts();">
                        <option value="|% r.id_rubroJuicio %|" ng-repeat="r in rubros" >|% r.rubro %|</option>
                      </select>
                  </div>
                </form>
              </p>              
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#j1" aria-controls="home" role="tab" data-toggle="tab" ng-click="loadData(1)">General</a></li>
                <li role="presentation"><a href="#j2" aria-controls="j2" role="tab" data-toggle="tab" ng-click="loadData(2)">Entradas</a></li>
                <li role="presentation"><a href="#j3" aria-controls="j3" role="tab" data-toggle="tab" ng-click="loadData(3)">Salidas</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content" >
                <div role="tabpanel" class="tab-pane active" id="j1" >
                  <canvas id="chartJ1" class="chart chart-line" data="J1.data"
                                labels="J1.labels" legend="true" series="J1.series"
                                click="onClick"></canvas> 
                </div>
                <div role="tabpanel" class="tab-pane" id="j2" >
                  <canvas id="chartJ2" class="chart chart-bar" data="J2.data"
                                labels="J2.labels" legend="true" series="J2.series"
                                click="onClick"></canvas> 
                </div>
                <div role="tabpanel" class="tab-pane" id="j3" >
                  <canvas id="chartJ3" class="chart chart-bar" data="J3.data"
                                labels="J3.labels" legend="true" series="J3.series"
                                click="onClick"></canvas> 
                </div>
              </div>
            </div>
          </div>
      </div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{URL::asset('js/Chart.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/angular-chart.min.js')}}"></script>
<script type="text/javascript">

$('.with-select').on('click', function(event) {
    event.stopPropagation();
});


angular.module("InformeApp", ['chart.js'],function($interpolateProvider) {
    $interpolateProvider.startSymbol('|%');
    $interpolateProvider.endSymbol('%|');
})
.constant("YEAR",'{{ $label_anyo }}')
.constant("PREFIJO", '{{ $label_juzgado }}')
.factory("JuicioService", function($http, YEAR, PREFIJO){
    return {
      getRubrosByJuicio : function(data){
        return $http.get('{{ URL::to("catalogos/juicios") }}/'+ data + '/rubros');
      },
      getBaseURL : function() {
        var urlbase = "";
        urlbase = (PREFIJO == "Todos") ? urlbase += "/global" : urlbase + "/juzgado/" + PREFIJO;
        if(YEAR != "Todos") { urlbase = urlbase + "/" + YEAR; }
        return urlbase;
      },
      getChartJuicioGeneral : function(juicio, rubro){
        var url = "{{ URL::to('charts/familiar') }}";
        var param = (rubro == 0) ? "?juicio=" + juicio : "?rubro=" + rubro; 
        url = url + this.getBaseURL() + param;
        return $http.get(url);
      },
      getChartJuicioEntradas : function(juicio, rubro){
        var url = "{{ URL::to('charts/familiar/entradas') }}";
        var param = (rubro == 0) ? "?juicio=" + juicio : "?rubro=" + rubro; 
        url = url + this.getBaseURL() + param;
        return $http.get(url);
      },
      getChartJuicioSalidas : function(juicio, rubro){
        var url = "{{ URL::to('charts/familiar/salidas') }}";
        var param = (rubro == 0) ? "?juicio=" + juicio : "?rubro=" + rubro; 
        url = url + this.getBaseURL() + param;
        return $http.get(url);
      }
    }
  })
.controller("ChangeController", function ($scope,$window){
  $scope.prefijo = "{{ $label_juzgado }}";
  $scope.anyo = "{{ $label_anyo }}";
  $scope.cambiarJuzgado = function(){
    var url = "{{ url('/informe/familiar') }}";
    url = ($scope.prefijo == "Todos" ) ? url += "/global" :  url + "/juzgado/" + $scope.prefijo;

    if($scope.anyo != "Todos")
    {
      url = url + "/" + $scope.anyo;
    }
    $window.location.href = url;
  };
})
.controller("GlobalController", function ($scope, $timeout) {
  $scope.loadG1 = function(){
    $timeout(function(){ 
      $scope.data =  <?php echo json_encode($charts["General"]->data)  ?> ;
      $scope.series = <?php echo json_encode($charts["General"]->series)  ?> ;
      $scope.labels = <?php echo json_encode($charts["General"]->etiquetas)  ?> ;
    });
    
  };

   $scope.loadG2 = function(){
    $timeout(function(){  
      $scope.data2 =  <?php echo json_encode($charts["Entradas"]->data)  ?> ;
      $scope.series2 = <?php echo json_encode($charts["Entradas"]->series)  ?> ;
      $scope.labels2 = <?php echo json_encode($charts["Entradas"]->etiquetas)  ?> ;
    });
    
  };

  $scope.loadG3 = function(){
    $timeout(function(){  
      $scope.data3 =  <?php echo json_encode($charts["Salidas"]->data)  ?> ;
      $scope.series3 = <?php echo json_encode($charts["Salidas"]->series)  ?> ;
      $scope.labels3 = <?php echo json_encode($charts["Salidas"]->etiquetas)  ?> ;
    });
    
  };

  $scope.loadG1();
}).controller("JuicioController", function ($scope,$timeout,JuicioService) {
    var chartJ1 = {'data': [], 'series' : [], 'etiquetas' : [] };
    var chartJ2  = {'data': [], 'series' : [], 'etiquetas' : [] };
    var chartJ3  = {'data': [], 'series' : [], 'etiquetas' : [] };    
    
    $scope.J1 = {'data': [], 'series' : [], 'etiquetas' : [] };
    $scope.J2  = {'data': [], 'series' : [], 'etiquetas' : [] };
    $scope.J3  = {'data': [], 'series' : [], 'etiquetas' : [] }; 
        
    $scope.active = 1;
    $scope.juicio = {{ $juicios->first()->id_juicio }};
    $scope.rubros = {!! $rubros !!};
    $scope.rubros.splice(0,0,{"id_rubroJuicio":0,"id_juicio":$scope.juicio,"rubro":"Todos"});
    $scope.rubro = 0;
    
    $scope.getChartJ1 = function() {
      JuicioService.getChartJuicioGeneral($scope.juicio, $scope.rubro)
      .success(function(data){
        $scope.setData(data, chartJ1, $scope.J1,1);
      })
      .error(function(){
        alert("Error");
      });
    };

    $scope.setData = function(ajaxData,tempData, scopeData, numchart ) {
      
        tempData.data = ajaxData.data;
        tempData.series = ajaxData.series;
        tempData.etiquetas = ajaxData.etiquetas;
        if($scope.active == numchart) $scope.load(tempData, scopeData);
     
    };

    $scope.load = function(tempData, scopeData){
      
        $timeout(function(){  
          scopeData.data = tempData.data;
          scopeData.series = tempData.series;
          scopeData.labels = tempData.etiquetas;
        });
      
    };
    

    $scope.getChartJ2 = function() {
      JuicioService.getChartJuicioEntradas($scope.juicio, $scope.rubro)
      .success(function(data){
        $scope.setData(data, chartJ2, $scope.J2,2);
      })
      .error(function(){
        alert("Error");
      });
    };


    $scope.getChartJ3 = function() {
      JuicioService.getChartJuicioSalidas($scope.juicio, $scope.rubro)
      .success(function(data){
        $scope.setData(data, chartJ3, $scope.J3,3);
      })
      .error(function(){
        alert("Error");
      });
    };

    $scope.getRubros = function(){
    JuicioService.getRubrosByJuicio($scope.juicio)
    .success(function(data){
      $scope.rubros = data;
      $scope.rubros.splice(0,0,{"id_rubroJuicio":0,"id_juicio":$scope.juicio,"rubro":"Todos"});
      $scope.rubro = 0;
      $scope.getCharts();
    })
    .error(function(){
      alert("Error");
    }); 
  };

    $scope.getCharts = function(){
      $scope.getChartJ1();
      $scope.getChartJ2();
      $scope.getChartJ3();
    };

    $scope.loadData = function(chart){
      $scope.active = chart;
      switch($scope.active){
        case 1:
          $scope.load(chartJ1, $scope.J1);
          break;
        case 2:
          $scope.load(chartJ2, $scope.J2);
          break;
        case 3:
          $scope.load(chartJ3, $scope.J3);
          break;
      }
    }

    $scope.getCharts();
});
</script>
@endsection