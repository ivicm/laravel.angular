@extends('app')

@section('content')
<div class="container" ng-app="SalidaFamiliarApp" ng-controller="SalidaFamiliarController">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Captura</a></li>
  		<li><a href="#">Familiar</a></li>
  		<li class="active"><a href="#">Salidas</a></li>
	</ol>
	<h1 class="page-header">Salida Familiar</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			
			<div class="alert |% alert.type %| alert-dismissible" role="alert" ng-show="alert.show">
  						|% alert.text %|
  			</div>			
			<form class="form-horizontal" method="post">
			  <div class="form-group">
			    <label for="juzgado" class="col-sm-2 control-label">Juzgado:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juzgado" ng-model="salida.juzgado" ng-change="getJuicios()">
			      	@foreach ($juzgados as $jz)
			      		<option value="{{ $jz->id_juzgado }}">{{ $jz->prefijo }}</option>
			      	@endforeach
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="anyo" class="col-sm-2 control-label">Año:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="anyo" ng-model="salida.anyo">
			      	@for($i = date("Y"); $i > 2011; $i--)
			      		<option value="{{ $i }}"> {{ $i }} </option>
			      	@endfor
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="mes" class="col-sm-2 control-label">Mes:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="tipo" name="tipo" ng-model="salida.mes">
						<option value="1">Enero</option>
						<option value="2">Febrero</option>
						<option value="3">Marzo</option>
						<option value="4">Abril</option>
						<option value="5">Mayo</option>
						<option value="6">Junio</option>
						<option value="7">Julio</option>
						<option value="8">Agosto</option>
						<option value="9">Septiembre</option>
						<option value="10">Octubre</option>
						<option value="11">Noviembre</option>
						<option value="12">Diciembre</option>
					  </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="juicio" class="col-sm-2 control-label">Juicio:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juicio" ng-model="juicio" ng-change="getRubros()">
			      		<option value="|% j.id_juicio %|" ng-repeat="j in juicios">|% j.juicio %| (|% j.materia.materia %|)</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="rubro" class="col-sm-2 control-label">Rubro:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="rubro" ng-model="salida.rubro">
			      		<option value="|% rubro.id_rubroJuicio %|" ng-repeat="rubro in rubros" >|% rubro.rubro %|</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			  	<label for="txtProcedente" class="col-sm-2 control-label">Sentencia procedente:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtProcedente" class="form-control" ng-model="salida.procedente" /> 
			  	</div>
			  </div>
			  <div class="form-group">
			  	<label for="txtImprocedente" class="col-sm-2 control-label">Sentencia improcedente:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtImprocedente" class="form-control" ng-model="salida.improcedente" /> 
			  	</div>
			  </div>
			  <div class="form-group">
			  	<label for="txtAuto" class="col-sm-2 control-label">Auto:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtAuto" class="form-control" ng-model="salida.auto" /> 
			  	</div>
			  </div>
			  <div class="form-group">
			  	<label for="txtExcusaRecusacion" class="col-sm-2 control-label">Excusa/Recusación:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtExcusaRecusacion" class="form-control" ng-model="salida.excusarecusacion" /> 
			  	</div>
			  </div>
			  <div class="form-group">
			  	<label for="txtCompetencia" class="col-sm-2 control-label">Competencia:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtCompetencia" class="form-control" ng-model="salida.competencia" /> 
			  	</div>
			  </div>
			  <div class="form-group">
			  	<label for="txtAcumulacion" class="col-sm-2 control-label">Acumulación:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtAcumulación" class="form-control" ng-model="salida.acumulacion" /> 
			  	</div>
			  </div>
			  <div class="form-group">
			  	<label for="txtAP" class="col-sm-2 control-label">Archivo provisional:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtAP" class="form-control" ng-model="salida.ap"/> 
			  	</div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="button" class="btn btn-primary" ng-click="sendData()">Agregar</button>
			    </div>
			  </div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
		angular.module('SalidaFamiliarApp', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('|%');
        $interpolateProvider.endSymbol('%|');
    })
  .factory('SalidaFamiliarService', function($http){
  	return {
  		getRubrosByJuicio : function(data){
  			return $http.get('{{ URL::to("catalogos/juicios") }}/'+ data + '/rubros');
  		},
  		insertSalidaFamiliar : function(data){
  			return $http.post('',data);
  		},
  		getJuiciosByTipoJuzgado : function(data) {
  			return $http.get('{{ URL::to("juzgados") }}/'+ data + '/juicios');
  		}
  	}
  })
  .controller('SalidaFamiliarController', function($scope, $interval, SalidaFamiliarService) {
    var fecha = new Date();
    $scope.rubros = {!! $rubros !!};
    $scope.juicios = {!! $juicios !!};
    $scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
    $scope.juicio = {{ $juicios->first()->id_juicio }};
    $scope.salida = {
    	'anyo': fecha.getFullYear(),
    	'mes' : fecha.getMonth()+1 ,
        'juzgado' : {{ $juzgados->first()->id_juzgado }},
        'rubro' : $scope.rubros[0].id_rubroJuicio,
        'procedente' : '',
        'improcedente' : '',
        'auto' : '',
        'excusarecusacion' : '',
        'competencia' : '',
        'acumulacion' : '',
        'ap' : ''
    }
 	
 	$scope.getRubros = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		SalidaFamiliarService.getRubrosByJuicio($scope.juicio)
 		.success(function(data){
 			$scope.rubros = data;
 			$scope.salida.rubro = $scope.rubros[0].id_rubroJuicio;
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

 	$scope.sendData = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		SalidaFamiliarService.insertSalidaFamiliar($scope.salida)
 		.success(function(response){
 			if(response.status == "OK")
 			{
 				$scope.alert = {'show' : true, 'text' : 'La información ha sido guardada.' + response.mensaje + ' ' + JSON.stringify($scope.salida), 'type' : 'alert-success'};
 				$scope.salida.procedente = 0;
 				$scope.salida.improcedente = 0;
 				$scope.salida.auto = 0;
 				$scope.salida.excusarecusacion = 0;
 				$scope.salida.competencia = 0;
 				$scope.salida.acumulacion = 0;
 				$scope.salida.ap = 0;
 				/*$interval(function(){
 					$scope.alert.show = false;
 				},10000);*/
 			}
 			else
 			{
 				$scope.alert = {'show' : true, 'text' : response.mensaje , 'type' : 'alert-warning'};	
 			}	
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});

 	};

 	$scope.getJuicios = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		SalidaFamiliarService.getJuiciosByTipoJuzgado($scope.salida.juzgado)
 		.success(function(data){
 			$scope.juicios = data;
 			$scope.juicio = $scope.juicios[0].id_juicio;
 			$scope.getRubros();
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

  });
</script>
@endsection