@extends('app')

@section('content')
<div class="container" ng-app="EntradaCivilApp" ng-controller="EntradaCivilController">
	<ol class="breadcrumb">
  		<li><a href="{{ URL::to('') }}">Inicio</a></li>
  		<li><a href="#">Captura</a></li>
  		<li><a href="#">Civil</a></li>
  		<li class="active"><a href="#">Entradas</a></li>
	</ol>
	<h1 class="page-header">Entrada Civil</h1>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			
			<div class="alert |% alert.type %| alert-dismissible" role="alert" ng-show="alert.show">
  						|% alert.text %|
  			</div>			
			<form class="form-horizontal" method="post">
			  <div class="form-group">
			    <label for="juzgado" class="col-sm-2 control-label">Juzgado:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juzgado" ng-model="entrada.juzgado" ng-change="getJuicios()">
			      	@foreach ($juzgados as $jz)
			      		<option value="{{ $jz->id_juzgado }}">{{ $jz->prefijo }}</option>
			      	@endforeach
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="anyo" class="col-sm-2 control-label">Año:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="anyo" ng-model="entrada.anyo">
			      	@for($i = date("Y"); $i > 2011; $i--)
			      		<option value="{{ $i }}"> {{ $i }} </option>
			      	@endfor
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="mes" class="col-sm-2 control-label">Mes:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="tipo" name="tipo" ng-model="entrada.mes">
						<option value="1">Enero</option>
						<option value="2">Febrero</option>
						<option value="3">Marzo</option>
						<option value="4">Abril</option>
						<option value="5">Mayo</option>
						<option value="6">Junio</option>
						<option value="7">Julio</option>
						<option value="8">Agosto</option>
						<option value="9">Septiembre</option>
						<option value="10">Octubre</option>
						<option value="11">Noviembre</option>
						<option value="12">Diciembre</option>
					  </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="juicio" class="col-sm-2 control-label">Juicio:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="juicio" ng-model="juicio" ng-change="getRubros()">
			      		<option value="|% j.id_juicio %|" ng-repeat="j in juicios">|% j.juicio %| (|% j.materia.materia %|)</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="rubro" class="col-sm-2 control-label">Rubro:</label>
			    <div class="col-sm-10">
			      <select class="form-control" id="rubro" ng-model="entrada.rubro">
			      		<option value="|% rubro.id_rubroJuicio %|" ng-repeat="rubro in rubros" >|% rubro.rubro %|</option>
			      </select>
			    </div>
			  </div>
			  <div class="form-group">
			  	<label for="txtIngreso" class="col-sm-2 control-label">Ingreso:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtIngreso" class="form-control" ng-model="entrada.ingreso" /> 
			  	</div>
			  </div>
			  <div class="form-group">
			  	<label for="txtReingreso" class="col-sm-2 control-label">Reingreso:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtReingreso" class="form-control" ng-model="entrada.reingreso" /> 
			  	</div>
			  </div>
			  <div class="form-group">
			  	<label for="txtAP" class="col-sm-2 control-label">Archivo provisional:</label>
			  	<div class="col-sm-2">
			  		<input type="text" id="txtAP" class="form-control" ng-model="entrada.ap"/> 
			  	</div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="button" class="btn btn-primary" ng-click="sendData()">Agregar</button>
			    </div>
			  </div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
		angular.module('EntradaCivilApp', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('|%');
        $interpolateProvider.endSymbol('%|');
    })
  .factory('EntradaCivilService', function($http){
  	return {
  		getRubrosByJuicio : function(data){
  			return $http.get('{{ URL::to("catalogos/juicios") }}/'+ data + '/rubros');
  		},
  		insertEntradaCivil : function(data){
  			return $http.post('',data);
  		},
  		getJuiciosByTipoJuzgado : function(data) {
  			return $http.get('{{ URL::to("juzgados") }}/'+ data + '/juicios');
  		}
  	}
  })
  .controller('EntradaCivilController', function($scope, $interval, EntradaCivilService) {
    var fecha = new Date();
    $scope.rubros = {!! $rubros !!};
    $scope.juicios = {!! $juicios !!};
    $scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
    $scope.juicio = {{ $juicios->first()->id_juicio }};
    $scope.entrada = {
    	'anyo': fecha.getFullYear(),
    	'mes' : fecha.getMonth()+1 ,
        'juzgado' : {{ $juzgados->first()->id_juzgado }},
        'rubro' : $scope.rubros[0].id_rubroJuicio,
        'ingreso' : '',
        'reingreso' : '',
        'ap' : ''
    }
 	
 	$scope.getRubros = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		EntradaCivilService.getRubrosByJuicio($scope.juicio)
 		.success(function(data){
 			$scope.rubros = data;
 			$scope.entrada.rubro = $scope.rubros[0].id_rubroJuicio;
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

 	$scope.sendData = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		EntradaCivilService.insertEntradaCivil($scope.entrada)
 		.success(function(response){
 			if(response.status == "OK")
 			{
 				$scope.alert = {'show' : true, 'text' : 'La información ha sido guardada.' + response.mensaje + ' ' + JSON.stringify($scope.entrada), 'type' : 'alert-success'};
 				$scope.entrada.ingreso = 0;
 				$scope.entrada.reingreso = 0;
 				$scope.entrada.ap = 0;
 				/*$interval(function(){
 					$scope.alert.show = false;
 				},10000);*/
 			}
 			else
 			{
 				$scope.alert = {'show' : true, 'text' : response.mensaje , 'type' : 'alert-warning'};	
 			}	
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});

 	};

 	$scope.getJuicios = function(){
 		$scope.alert = {'show' : false, 'text' : '...', 'type' : ''};
 		EntradaCivilService.getJuiciosByTipoJuzgado($scope.entrada.juzgado)
 		.success(function(data){
 			$scope.juicios = data;
 			$scope.juicio = $scope.juicios[0].id_juicio;
 			$scope.getRubros();
 		})
 		.error(function(){
 			$scope.alert = {'show' : true, 'text' : 'Ha ocurrido un error' , 'type' : 'alert-danger'};	
 		});	
 	};

  });
</script>
@endsection