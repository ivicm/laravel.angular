<?php

use App\Services\ChartsCivilService;
use App\ChartScopes;

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$response = $this->call('GET', '/');

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testChartGeneralTotal()
	{
		$s = new ChartsCivilService();
		$c = $s->getGeneralTotal();
		$r = false;

		if($c->titulo = "General total")
		{
			$r = true;
		}

		$this->assertTrue($r);
	}

	public function testGlobalGeneralChart()
	{
		$s = new ChartScopes\GlobalGeneralChart;
		$c = $s->getTotal();
		$r = false;

		if($c->titulo = "General total")
		{
			$r = true;
		}

		$this->assertTrue($r);
	}

}
