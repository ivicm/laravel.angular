<?php namespace App\Repositories;

use App\Models;
use App\Contracts;

class RepositorioEntradasFamiliar implements Contracts\IRepositorioFamiliar {

	public function getReporteJuzgadoByRubroDesgloseAnual($id_juzgado, $id_rubroJuicio, $anyo){
		$entradas = Models\EntradaFamiliar :: selectRaw("mes as fila ,ingreso, reingreso, archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("id_rubroJuicio","=",$id_rubroJuicio)
                ->where("año", "=", $anyo)
                ->orderBy("mes")
                ->get();
        
        return $entradas;
	}

	public function getReporteJuzgadoByRubroAnual($id_juzgado, $id_rubroJuicio, $anyo) {
		$entradas = Models\EntradaFamiliar :: selectRaw(" año as fila , sum(ingreso) as ingreso, sum(reingreso) as reingreso, sum(archivoprovisional) as archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("id_rubroJuicio","=",$id_rubroJuicio)
                ->where("año", "=", $anyo)
                ->get();
        
        return $entradas;	
	}

	public function getReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo){
		$entradas = Models\EntradaFamiliar :: whereHas('RubroJuicio', function($q) use ($id_juicio) {
                    $q->where("id_juicio","=",$id_juicio);
                })
                ->selectRaw("año as fila, sum(ingreso) as ingreso, sum(reingreso) as reingreso, sum(archivoprovisional) as archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("año", "=", $anyo)
                ->get();
        return $entradas;
	}

	public function getReporteJuzgadoByJuicioDesgloseAnual($id_juzgado, $id_juicio, $anyo){
		$entradas = Models\EntradaFamiliar :: whereHas('RubroJuicio', function($q) use ($id_juicio) {
                    $q->where("id_juicio","=",$id_juicio);
                })
                ->selectRaw("mes as fila, sum(ingreso) as ingreso, sum(reingreso) as reingreso, sum(archivoprovisional) as archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes")
                ->get();
        return $entradas;
	}
}