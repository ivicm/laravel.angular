<?php namespace App\Repositories;

use App\Models;

class RepositorioCatalogos {

	public function getJuzgadosRamaCivil(){
		$juzgados = \App\Models\Juzgado :: where('id_tipojuzgado',1)
		                                   ->orWhere('id_tipojuzgado',2)
		                                   ->orWhere('id_tipojuzgado',5)
		                                   ->get();	
		return $juzgados;
	}

	public function getJuzgadosRamaFamiliar(){
		$juzgados = \App\Models\Juzgado :: where('id_tipojuzgado',4)
		                                   ->get();	
		return $juzgados;
	}	

	public function getJuzgadoByPrefijo($prefijo){
		$juzgado = \App\Models\Juzgado :: where('prefijo',$prefijo) 
	                ->where('tipo','<>','P')
	                ->first();
	    return $juzgado;	
	}

	public function getJuzgadoById($id_juzgado){
		$juzgado = \App\Models\Juzgado :: find($id_juzgado);
	    return $juzgado;	
	}

	public function getJuicioById($id_juicio){
		$juicio = \App\Models\Juicio :: find($id_juicio);
		return $juicio;
	}

	public function getRubroJuicioById($id_rubro) {
		$rubro = \App\Models\RubroJuicio :: find($id_rubro);
		return $rubro;
	}

	public function getMeses() {
		$meses = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		return $meses;
	}
}