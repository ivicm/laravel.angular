<?php namespace App\Repositories;

use App\Models;
use App\Contracts;

class RepositorioSalidasCivil implements Contracts\IRepositorioCivil {

	public function getReporteJuzgadoByRubroDesgloseAnual($id_juzgado, $id_rubroJuicio, $anyo){
		$salidas = Models\SalidaCivil :: selectRaw("mes as fila
                                                      , sprocedente
                                                      , simprocedente
                                                      , auto
                                                      , excusa
                                                      , recusacion
                                                      , acumulacion
                                                      , archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("id_rubroJuicio","=",$id_rubroJuicio)
                ->where("año", "=", $anyo)
                ->orderBy("mes")
                ->get();
        
        return $salidas;
	}

	public function getReporteJuzgadoByRubroAnual($id_juzgado, $id_rubroJuicio, $anyo) {
		$salidas = Models\SalidaCivil :: selectRaw("año as fila
                                                      , sum(sprocedente) as sprocedente
                                                      , sum(simprocedente) as simprocedente
                                                      , sum(auto) as auto
                                                      , sum(excusa) as excusa
                                                      , sum(recusacion) as recusacion
                                                      , sum(acumulacion) as acumulacion
                                                      , sum(archivoprovisional) as archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("id_rubroJuicio","=",$id_rubroJuicio)
                ->where("año", "=", $anyo)
                ->get();
        
        return $salidas;	
	}

	public function getReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo){
		$salidas = Models\SalidaCivil :: whereHas('RubroJuicio', function($q) use ($id_juicio) {
                    $q->where("id_juicio","=",$id_juicio);
                })
                ->selectRaw("año as fila
                            , sum(sprocedente) as sprocedente
                            , sum(simprocedente) as simprocedente
                            , sum(auto) as auto
                            , sum(excusa) as excusa
                            , sum(recusacion) as recusacion
                            , sum(acumulacion) as acumulacion
                            , sum(archivoprovisional) as archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("año", "=", $anyo)
                ->get();
    return $salidas;
	}

	public function getReporteJuzgadoByJuicioDesgloseAnual($id_juzgado, $id_juicio, $anyo){
		$salidas = Models\SalidaCivil :: whereHas('RubroJuicio', function($q) use ($id_juicio) {
                    $q->where("id_juicio","=",$id_juicio);
                })
                ->selectRaw("mes as fila
                            , sum(sprocedente) as sprocedente
                            , sum(simprocedente) as simprocedente
                            , sum(auto) as auto
                            , sum(excusa) as excusa
                            , sum(recusacion) as recusacion
                            , sum(acumulacion) as acumulacion
                            , sum(archivoprovisional) as archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes")
                ->get();
    return $salidas;
	}
}