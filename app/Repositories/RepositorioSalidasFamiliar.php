<?php namespace App\Repositories;

use App\Models;
use App\Contracts;

class RepositorioSalidasFamiliar implements Contracts\IRepositorioFamiliar {

	public function getReporteJuzgadoByRubroDesgloseAnual($id_juzgado, $id_rubroJuicio, $anyo){
		$salidas = Models\SalidaFamiliar :: selectRaw("mes as fila
                                                      , sprocedente
                                                      , simprocedente
                                                      , auto
                                                      , excusarecusacion
                                                      , competencia
                                                      , acumulacion
                                                      , archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("id_rubroJuicio","=",$id_rubroJuicio)
                ->where("año", "=", $anyo)
                ->orderBy("mes")
                ->get();
        
        return $salidas;
	}

	public function getReporteJuzgadoByRubroAnual($id_juzgado, $id_rubroJuicio, $anyo) {
		$salidas = Models\SalidaFamiliar :: selectRaw("año as fila
                                                      , sum(sprocedente) as sprocedente
                                                      , sum(simprocedente) as simprocedente
                                                      , sum(auto) as auto
                                                      , sum(excusarecusacion) as excusarecusacion
                                                      , sum(competencia) as competencia
                                                      , sum(acumulacion) as acumulacion
                                                      , sum(archivoprovisional) as archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("id_rubroJuicio","=",$id_rubroJuicio)
                ->where("año", "=", $anyo)
                ->get();
        
        return $salidas;	
	}

	public function getReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo){
		$salidas = Models\SalidaFamiliar :: whereHas('RubroJuicio', function($q) use ($id_juicio) {
                    $q->where("id_juicio","=",$id_juicio);
                })
                ->selectRaw("año as fila
                            , sum(sprocedente) as sprocedente
                            , sum(simprocedente) as simprocedente
                            , sum(auto) as auto
                            , sum(excusarecusacion) as excusarecusacion
                            , sum(competencia) as competencia
                            , sum(acumulacion) as acumulacion
                            , sum(archivoprovisional) as archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("año", "=", $anyo)
                ->get();
    return $salidas;
	}

	public function getReporteJuzgadoByJuicioDesgloseAnual($id_juzgado, $id_juicio, $anyo){
		$salidas = Models\SalidaFamiliar :: whereHas('RubroJuicio', function($q) use ($id_juicio) {
                    $q->where("id_juicio","=",$id_juicio);
                })
                ->selectRaw("mes as fila
                            , sum(sprocedente) as sprocedente
                            , sum(simprocedente) as simprocedente
                            , sum(auto) as auto
                            , sum(excusarecusacion) as excusarecusacion
                            , sum(competencia) as competencia
                            , sum(acumulacion) as acumulacion
                            , sum(archivoprovisional) as archivoprovisional")
                ->where("id_juzgado","=", $id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes")
                ->get();
    return $salidas;
	}
}