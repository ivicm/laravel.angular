<?php namespace App\ChartScopes;

use App\Models;
use App\Contracts;
use App\ChartInfo;

class MateriaDesgloseSalidasChart implements Contracts\IDesgloseSalidasCivilChart {
    private $_materia;

    public function __construct(\App\Models\Materia $materia)
    {
        $this->_materia = $materia;
    }

    public function getTotal(){
    	 
        $salidas = \App\Models\SalidaCivil :: whereHas('RubroJuicio', function($q) {
                    $q->whereHas('Juicio', function($q2){
                        $q2->where('id_materia',"=",$this->_materia->id_materia);
                    });
                })
                ->selectRaw("año, 
                     CAST(sum(sprocedente)+ sum(simprocedente) AS SIGNED) as sentencias
                     , CAST(sum(auto) AS SIGNED) as autos
                     , CAST(sum(excusa) + sum(recusacion) + sum(acumulacion) AS SIGNED) as era
                     , CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->groupBy("año")
                ->orderBy("año","asc")
                ->get();
        if (!$salidas->isEmpty()) { 
            $labels = $salidas->lists("año");
            $data = array($salidas->lists("sentencias"),
                      $salidas->lists("autos"),
                      $salidas->lists("era"),
                      $salidas->lists("ap"));
        }
        else {
            $labels = array();
            $data = array(array(0),array(0),array(0),array(0));
        }
        $series = array("Sentencias", "Autos", "Excusa, Recusación y Acumulación" , "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Desglose salidas";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

	public function getAnual($anyo){
		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $sentencias = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $autos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $era = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $salidas = \App\Models\SalidaCivil :: whereHas('RubroJuicio', function($q) {
                    $q->whereHas('Juicio', function($q2){
                        $q2->where('id_materia',"=",$this->_materia->id_materia);
                    });
                })
                ->selectRaw("mes, 
                CAST(sum(sprocedente)+ sum(simprocedente) AS SIGNED) as sentencias
                     , CAST(sum(auto) AS SIGNED) as autos
                     , CAST(sum(excusa) + sum(recusacion) + sum(acumulacion) AS SIGNED) as era
                , CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","asc")
                ->get();
        foreach ($salidas as $s) {
            $sentencias[($s->mes)-1] = $s->sentencias;
            $autos[($s->mes)-1] = $s->autos;
            $era[($s->mes)-1] = $s->era;
            $ap[($s->mes)-1] = $s->ap;
        }
        $data = array( $sentencias, $autos, $era, $ap);
        $series = array("Sentencias", "Autos", "Excusa, Recusación y Acumulación" , "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Salidas anual ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}

	public function getByJuzgadoTotal(\App\Models\Juzgado $juzgado){
        $salidas = \App\Models\SalidaCivil :: whereHas('RubroJuicio', function($q) {
                    $q->whereHas('Juicio', function($q2){
                        $q2->where('id_materia',"=",$this->_materia->id_materia);
                    });
                })
                ->selectRaw("año, 
                       CAST(sum(sprocedente)+ sum(simprocedente) AS SIGNED) as sentencias
                     , CAST(sum(auto) AS SIGNED) as autos
                     , CAST(sum(excusa) + sum(recusacion) + sum(acumulacion) AS SIGNED) as era
                    , CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->groupBy("año")
                ->orderBy("año","asc")
                ->get();
         if (!$salidas->isEmpty()) { 
        $labels = $salidas->lists("año");
        $data = array($salidas->lists("sentencias"),
                      $salidas->lists("autos"),
                      $salidas->lists("era"),
                      $salidas->lists("ap"));
        }
        else {
            $labels = array();
            $data = array(array(0),array(0),array(0),array(0));
        }
        $series = array("Sentencias", "Autos", "Excusa, Recusación y Acumulación" , "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo."salidas total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}

	public function getByJuzgadoAnual(\App\Models\Juzgado $juzgado, $anyo){
		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        
        $sentencias = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $autos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $era = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $salidas = \App\Models\SalidaCivil :: whereHas('RubroJuicio', function($q) {
                    $q->whereHas('Juicio', function($q2){
                        $q2->where('id_materia',"=",$this->_materia->id_materia);
                    });
                })
                ->selectRaw("mes, 
                       CAST(sum(sprocedente)+ sum(simprocedente) AS SIGNED) as sentencias
                     , CAST(sum(auto) AS SIGNED) as autos
                     , CAST(sum(excusa) + sum(recusacion) + sum(acumulacion) AS SIGNED) as era
                     , CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","asc")
                ->get();
        foreach ($salidas as $s) {
            $sentencias[($s->mes)-1] = $s->sentencias;
            $autos[($s->mes)-1] = $s->autos;
            $era[($s->mes)-1] = $s->era;
            $ap[($s->mes)-1] = $s->ap;
        }
        $data = array( $sentencias, $autos, $era, $ap);
        $series = array("Sentencias", "Autos", "Excusa, Recusación y Acumulación" , "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}
}