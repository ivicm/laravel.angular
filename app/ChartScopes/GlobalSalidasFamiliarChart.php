<?php namespace App\ChartScopes;

use App\Models;
use App\Contracts;
use App\ChartInfo;

class GlobalSalidasFamiliarChart implements Contracts\ISalidasFamiliarChart {

	public function __construct()
    {
    
    }

    public function getTotal(){
        $data = array();
        $total = \App\Models\SalidaFamiliar :: selectRaw("año, 
                                                      CAST(sum(sprocedente)
                                                        +sum(simprocedente)
                                                        +sum(auto)
                                                        +sum(excusarecusacion)
                                                        +sum(competencia)
                                                        +sum(acumulacion)
                                                        +sum(archivoprovisional) as SIGNED 
                                                       )as total")
                ->groupBy("año")
                ->orderBy("año","asc")
                ->get();
        if (!$total->isEmpty()) { 
            $labels = $total->lists("año");
            $data[] = $total->lists("total");
        }
        else {
            $labels = array();
            $data[] = array(0);
        }
        $series = array("Salidas");
        
        $chart = new ChartInfo();

        $chart->titulo = "General total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

	public function getAnual($anyo) {
		 $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $data = array();
        $egresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $total = \App\Models\SalidaFamiliar :: selectRaw("mes, 
                                                        CAST(sum(sprocedente)
                                                        +sum(simprocedente)
                                                        +sum(auto)
                                                        +sum(excusarecusacion)
                                                        +sum(competencia)
                                                        +sum(acumulacion)
                                                        +sum(archivoprovisional) as SIGNED 
                                                       )as total")
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","asc")
                ->get();
        foreach ($total as $t) {
            $egresos[($t->mes)-1] = $t->total;
        }
        $data[] = $egresos;
        $series = array("Salidas");
        
        $chart = new ChartInfo();

        $chart->titulo = "General anual ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}
	public function getByJuzgadoTotal(\App\Models\Juzgado $juzgado){
        $data = array();
        $total = \App\Models\SalidaFamiliar :: selectRaw("año, 
                                                         CAST(sum(sprocedente)
                                                        +sum(simprocedente)
                                                        +sum(auto)
                                                        +sum(excusarecusacion)
                                                        +sum(competencia)
                                                        +sum(acumulacion)
                                                        +sum(archivoprovisional) as SIGNED 
                                                        )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->groupBy("año")
                ->orderBy("año","asc")
                ->get();
        if (!$total->isEmpty()) { 
            $labels = $total->lists("año");
            $data[] = $total->lists("total");
        }
        else {
            $labels = array();
            $data[] = array(0);
        }
        $series = array("Salidas");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}
	
	public function getByJuzgadoAnual(\App\Models\Juzgado $juzgado, $anyo){
		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $data = array();
        $egresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $total = \App\Models\SalidaFamiliar :: selectRaw("mes, 
                                                       CAST(sum(sprocedente)
                                                        +sum(simprocedente)
                                                        +sum(auto)
                                                        +sum(excusarecusacion)
                                                        +sum(competencia)
                                                        +sum(acumulacion)
                                                        +sum(archivoprovisional) as SIGNED 
                                                        )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","asc")
                ->get();
        foreach ($total as $t) {
            $egresos[($t->mes)-1] = $t->total;
        }
        $data[] = $egresos;
        $series = array("Salidas");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}

}