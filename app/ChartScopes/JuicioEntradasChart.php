<?php namespace App\ChartScopes;

use App\Models;
use App\Contracts;
use App\ChartInfo;

class JuicioEntradasChart implements Contracts\IEntradasCivilChart {

	private $_juicio;

	public function __construct(\App\Models\Juicio $juicio)
    {
    	$this->_juicio = $juicio;
    }

	public function getTotal(){
		$entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) {
                    $q->where("id_juicio","=",$this->_juicio->id_juicio);
                })
                ->selectRaw("año, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        if (!$entradas->isEmpty()) { 
            $labels = $entradas->lists("año");
            $data = array($entradas->lists("total"));
        }
        else {
            $labels = array();
            $data[] = array(0);
        }
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas por juicio";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
	}

	public function getAnual($anyo){
		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) {
                    $q->where("id_juicio","=",$this->_juicio->id_juicio);
                })
                ->selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas por juicio en el año ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
	}

	public function getByJuzgadoTotal(\App\Models\Juzgado $juzgado){
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) {
                    $q->where("id_juicio","=",$this->_juicio->id_juicio);
                })
                ->selectRaw("año, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $labels = $entradas->lists("año");
        if (!$entradas->isEmpty()) { 
            $data = array($entradas->lists("total"));
            $series = array("Entradas");
        }
        else {
            $labels = array();
            $data[] = array(0);
        }
        $chart = new ChartInfo();

        $chart->titulo = "Entradas en ".$juzgado->prefijo." por juicio";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }
	public function getByJuzgadoAnual(\App\Models\Juzgado $juzgado, $anyo){
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) {
                    $q->where("id_juicio","=",$this->_juicio->id_juicio);
                })
                ->selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas en ".$juzgado->prefijo ." por juicio en el año ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }
}