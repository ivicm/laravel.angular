<?php namespace App\ChartScopes;

use App\Models;
use App\Contracts;
use App\ChartInfo;

class GlobalDesgloseEntradasFamiliarChart implements Contracts\IDesgloseEntradasFamiliarChart {
    public function getTotal(){
    	 
        $entradas = \App\Models\EntradaFamiliar :: selectRaw("año, CAST(sum(ingreso) AS SIGNED) as ingresos, CAST(sum(reingreso) AS SIGNED) as reingresos, CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->groupBy("año")
                ->orderBy("año","asc")
                ->get();
        if(!$entradas->isEmpty())
        {
            $labels = $entradas->lists("año");
            $data = array($entradas->lists("ingresos"),
                      $entradas->lists("reingresos"),
                      $entradas->lists("ap"));
        }
        else {
            $labels = array();
            $data = array(array(0),array(0),array(0));
        }
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Desglose entradas";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

	public function getAnual($anyo){
		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $reingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaFamiliar :: selectRaw("mes, 
                CAST(sum(ingreso) AS SIGNED) as ingresos, 
                CAST(sum(reingreso) AS SIGNED) as reingresos,
                CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","asc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->ingresos;
            $reingresos[($e->mes)-1] = $e->reingresos;
            $ap[($e->mes)-1] = $e->ap;
        }
        $data = array( $ingresos, $reingresos, $ap);
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas anual ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}

	public function getByJuzgadoTotal(\App\Models\Juzgado $juzgado){
        $entradas = \App\Models\EntradaFamiliar :: selectRaw("año, CAST(sum(ingreso) AS SIGNED) as ingresos,
                                                        CAST(sum(reingreso) AS SIGNED) as reingresos,
                                                        CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->groupBy("año")
                ->orderBy("año","asc")
                ->get();
        if(!$entradas->isEmpty())
        {
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("ingresos"),
                      $entradas->lists("reingresos"),
                      $entradas->lists("ap"));
        }
        else {
            $labels = array();
            $data = array(array(0),array(0),array(0));
        }
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo."entradas total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}

	public function getByJuzgadoAnual(\App\Models\Juzgado $juzgado, $anyo){
		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $reingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaFamiliar :: selectRaw("mes, 
                                 CAST(sum(ingreso) AS SIGNED) as ingresos, 
                                 CAST(sum(reingreso) AS SIGNED) as reingresos,
                                 CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","asc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->ingresos;
            $reingresos[($e->mes)-1] = $e->reingresos;
            $ap[($e->mes)-1] = $e->ap;
        }
        $data = array( $ingresos, $reingresos, $ap);
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}
}