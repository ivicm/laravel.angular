<?php namespace App\ChartScopes;

use App\Models;
use App\Contracts;
use App\ChartInfo;

class GeneralFamiliarChart implements Contracts\IGeneralFamiliarChart {

    private $_entradas;
    private $_salidas;
	public function __construct(Contracts\IEntradasFamiliarChart $entradasChart, Contracts\ISalidasFamiliarChart $salidasChart)
    {
        $this->_entradas = $entradasChart;
        $this->_salidas = $salidasChart;
    }

    public function getTotal(){
        $e = $this->_entradas->getTotal();
        $s = $this->_salidas->getTotal();

        $series = array("Entradas", "Salidas");
        $labels = $this->orderLabels($e->etiquetas, $s->etiquetas);

        $entradas = array();
        $salidas = array();
        foreach ($labels as $l) {
            $i = array_search($l, $e->etiquetas);
            $j = array_search($l, $s->etiquetas);
            $entradas[] = $e->data[0][$i];
            $salidas[] = $s->data[0][$j];
        }

        $data = array($entradas, $salidas);
    	
        $chart = new ChartInfo();

        $chart->titulo = "General total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

	public function getAnual($anyo) {
        $e = $this->_entradas->getAnual($anyo);
        $s = $this->_salidas->getAnual($anyo);

		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $data = array($e->data[0],$s->data[0]);
        $series = array("Entradas", "Salidas");
               
        $chart = new ChartInfo();

        $chart->titulo = "General anual ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}
	public function getByJuzgadoTotal(\App\Models\Juzgado $juzgado){
        $e = $this->_entradas->getByJuzgadoTotal($juzgado);
        $s = $this->_salidas->getByJuzgadoTotal($juzgado);

        $series = array("Entradas", "Salidas");
        $labels = $this->orderLabels($e->etiquetas, $s->etiquetas);

        $entradas = array();
        $salidas = array();
        foreach ($labels as $l) {
            $i = array_search($l, $e->etiquetas);
            $j = array_search($l, $s->etiquetas);
            $entradas[] = $e->data[0][$i];
            $salidas[] = $s->data[0][$j];
        }

        $data = array($entradas, $salidas);

		/*$labels = \App\Models\EntradaCivil :: distinct()
                 ->selectRaw("CAST(año as CHAR(4)) as year")
                 ->where("id_juzgado","=", $juzgado->id_juzgado)
                 ->orderBy("año","desc")
                 //->get()
                 ->lists("year");
        $data = array();
        $total = \App\Models\EntradaCivil :: selectRaw("CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->groupBy("año")
                ->orderBy("año","desc")
                //->get();
                ->lists("total");
        $data[] = $total;
        $series = array("Entradas");*/
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}
	
	public function getByJuzgadoAnual(\App\Models\Juzgado $juzgado, $anyo){
        $e = $this->_entradas->getByJuzgadoAnual($juzgado, $anyo);
        $s = $this->_salidas->getByJuzgadoAnual($juzgado, $anyo);

		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $data = array($e->data[0],$s->data[0]);
        
        $series = array("Entradas", "Salidas");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
	}

    private function orderLabels($labels1, $labels2) {
        $labels = array_merge($labels1, array_diff($labels2, $labels1));
        rsort($labels);
        return $labels;
    }
}