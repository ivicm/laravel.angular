<?php namespace App\ChartScopes;

use App\Models;
use App\Contracts;
use App\ChartInfo;

class RubroEntradasChart implements Contracts\IEntradasCivilChart {
	private $_rubro;

	public function __construct(\App\Models\RubroJuicio $rubro)
    {
    	$this->_rubro = $rubro;
    }

    public function getTotal(){
    	$entradas = \App\Models\EntradaCivil :: selectRaw("año, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_rubroJuicio","=",$this->_rubro->id_rubroJuicio)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        if (!$entradas->isEmpty()) { 
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("total"));
    } else {
        $labels = array();
        $data[] = array(0);
    }
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas por rubro";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

	public function getAnual($anyo){
		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("año","=",$anyo)
                ->where("id_rubroJuicio","=",$this->_rubro->id_rubroJuicio)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas por rubro en el año ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
	}

	public function getByJuzgadoTotal(\App\Models\Juzgado $juzgado){
		$entradas = \App\Models\EntradaCivil :: selectRaw("año, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("id_rubroJuicio","=",$this->_rubro->id_rubroJuicio)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        if (!$entradas->isEmpty()) { 
            $labels = $entradas->lists("año");
            $data = array($entradas->lists("total"));
        } else {
            $labels = array();
            $data[] = array(0);
        }
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas por ".$this->_rubro->rubro." ";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
	}

	public function getByJuzgadoAnual(\App\Models\Juzgado $juzgado, $anyo){
		$labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("id_rubroJuicio","=",$this->_rubro->id_rubroJuicio)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas en ".$juzgado->prefijo ." por rubro en el año ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
	}
}