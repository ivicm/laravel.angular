<?php namespace App\ChartScopes;

use App\Models;
use App\Contracts;
use App\ChartInfo;

class MateriaGeneralChart implements Contracts\IGeneralCivilChart {
	private $_materia;
    private $_entradas;
    private $_salidas;

	public function __construct(\App\Models\Materia $materia)
    {
    	$this->_materia = $materia;
        $this->_entradas = new MateriaEntradasChart($materia);
    }

    public function getTotal(){
    	
        
        //$chart = new ChartInfo();

        //$chart->titulo = "Entradas por rubro";
        //$chart->etiquetas = $labels;
        //$chart->series = $series;
        //$chart->data = $data;

        //return $chart;
        return $this->_entradas->getTotal();
    }

	public function getAnual($anyo){
		
        
        //$chart = new ChartInfo();

        //$chart->titulo = "Entradas por rubro en el año ".$anyo;
        //$chart->etiquetas = $labels;
        //$chart->series = $series;
        //$chart->data = $data;

        //return $chart;   
        return $this->_entradas->getAnual($anyo);
	}

	public function getByJuzgadoTotal(\App\Models\Juzgado $juzgado){
		
        //$chart = new ChartInfo();

        //$chart->titulo = "Entradas por ".$this->_rubro->rubro." ";
        //$chart->etiquetas = $labels;
        //$chart->series = $series;
        //$chart->data = $data;

        //return $chart;   
        return $this->_entradas->getByJuzgadoTotal($juzgado);
	}

	public function getByJuzgadoAnual(\App\Models\Juzgado $juzgado, $anyo){
		        
        //$chart = new ChartInfo();

        //$chart->titulo = "Entradas en ".$juzgado->prefijo ." por rubro en el año ".$anyo;
        //$chart->etiquetas = $labels;
        //$chart->series = $series;
        //$chart->data = $data;

        return $this->_entradas->getByJuzgadoAnual($juzgado, $anyo);   
	}
}