<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model{
	protected $primaryKey = 'id_materia';
	public $timestamps = false;
	protected $connection = 'mysql';
	protected $table = 'cat_materias';

	public function juicios(){
		return $this->hasMany('App\Models\Juicio','id_materia');
	}

	public function tipojuzgados()
	{
		return $this->belongsToMany('App\Models\TipoJuzgado', 'pivot_materias_tipojuzgados', 'id_materia', 'id_tipojuzgado');
	}
}