<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RubroJuicio extends Model{
	protected $primaryKey = 'id_rubroJuicio';
	public $timestamps = false;
	protected $connection = 'mysql';
	protected $table = 'cat_rubrojuicios';

	public function juicio(){
		return $this->belongsTo('App\Models\Juicio','id_juicio');
	}
}