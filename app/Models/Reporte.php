<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model{
	protected $primaryKey = 'id_reporte';
	public $timestamps = false;
	protected $connection = 'mysql';
	protected $table = 'cat_reportes';
}