<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntradaFamiliar extends Model{
	protected $primaryKey = 'id_entradas_familiar';
	public $timestamps = false;
	protected $connection = 'mysql';
	protected $table = 'entradas_familiar';

	public function Juzgado(){
		return $this->belongsTo('App\Models\Juzgado','id_juzgado');
	}

	public function RubroJuicio(){
		return $this->belongsTo('App\Models\RubroJuicio','id_rubroJuicio');	
	}
}