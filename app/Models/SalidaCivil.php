<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalidaCivil extends Model{
	protected $primaryKey = 'id_salidas_civil';
	public $timestamps = false;
	protected $connection = 'mysql';
	protected $table = 'salidas_civil';

	public function Juzgado(){
		return $this->belongsTo('App\Models\Juzgado','id_juzgado');
	}

	public function RubroJuicio(){
		return $this->belongsTo('App\Models\RubroJuicio','id_rubroJuicio');	
	}
}