<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Juzgado extends Model{
	protected $primaryKey = 'id_juzgado';
	public $timestamps = false;
	protected $connection = 'mysql';
	protected $table = 'cat_juzgados';

	public function tipojuzgado(){
		return $this->belongsTo('App\Models\TipoJuzgado','id_tipojuzgado');
	}
}