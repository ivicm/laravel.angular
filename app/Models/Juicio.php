<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Juicio extends Model{
	protected $primaryKey = 'id_juicio';
	public $timestamps = false;
	protected $connection = 'mysql';
	protected $table = 'cat_juicios';

	public function rubroJuicios(){
		return $this->hasMany('App\Models\RubroJuicio','id_juicio');
	}

	public function materia(){
		return $this->belongsTo('App\Models\Materia','id_materia');
	}
}