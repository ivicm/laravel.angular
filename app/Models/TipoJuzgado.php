<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoJuzgado extends Model{
	protected $primaryKey = 'id_tipojuzgado';
	public $timestamps = false;
	protected $connection = 'mysql';
	protected $table = 'cat_tipojuzgados';

	public function materias()
	{
		return $this->belongsToMany('App\Models\Materia', 'pivot_materias_tipojuzgados', 'id_tipojuzgado', 'id_materia');
	}


	public function getJuicios()
	{
		$juicios = array();
		foreach($this->materias as $m)
		{
			foreach ($m->juicios as $j) {
				$juicios[] = $j->load('materia');
			}
		}
		return \Illuminate\Database\Eloquent\Collection::make($juicios);
	}
}