<?php namespace App\Services;

use App\ChartInfo;
use App\Models;
/**
* Our PokemonService, containing all useful methods for business logic around Pokemon
*/
class ChartsService
{
        
    
    public function __construct()
    {
    
    }

    public function getCivilGeneralTotal()
    {
        $labels = \App\Models\EntradaCivil :: distinct()
                 ->selectRaw("CAST(año as CHAR(4)) as year")
                 ->orderBy("año","desc")
                 //->get()
                 ->lists("year");
        $data = array();
        $total = \App\Models\EntradaCivil :: selectRaw("CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->groupBy("año")
                ->orderBy("año","desc")
                //->get();
                ->lists("total");
        $data[] = $total;
        $series = json_encode(array("Entradas"));
        $labelsjson = json_encode($labels);

        $chart = new ChartInfo();

        $chart->titulo = "General total";
        $chart->etiquetas = $labelsjson;
        $chart->series = $series;
        $chart->data = json_encode($data);

        return $chart;
    }
}