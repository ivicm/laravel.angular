<?php namespace App\Services;

use App\ChartInfo;
use App\Models;
use App\Contracts;
use App\ChartScopes;
/**
* 
*/
class GlobalFamiliarChartsService extends Contracts\AbsFamiliarChartService
{
        
    public function __construct()
    {
    	//$this->general = new ChartScopes\GlobalGeneralChart();
    	$this->general = new ChartScopes\GeneralFamiliarChart(new ChartScopes\GlobalEntradasFamiliarChart()
    		                                         , new ChartScopes\GlobalSalidasFamiliarChart());
    	$this->entradas = new ChartScopes\GlobalDesgloseEntradasFamiliarChart();
    	$this->salidas = new ChartScopes\GlobalDesgloseSalidasFamiliarChart(); 
    }
}