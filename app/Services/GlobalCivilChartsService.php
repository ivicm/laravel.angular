<?php namespace App\Services;

use App\ChartInfo;
use App\Models;
use App\Contracts;
use App\ChartScopes;
/**
* 
*/
class GlobalCivilChartsService extends Contracts\AbsCivilChartService
{
        
    public function __construct()
    {
    	//$this->general = new ChartScopes\GlobalGeneralChart();
    	$this->general = new ChartScopes\GeneralChart(new ChartScopes\GlobalEntradasChart()
    		                                         , new ChartScopes\GlobalSalidasChart());
    	$this->entradas = new ChartScopes\GlobalDesgloseEntradasChart();
    	$this->salidas = new ChartScopes\GlobalDesgloseSalidasChart(); 
    }
}