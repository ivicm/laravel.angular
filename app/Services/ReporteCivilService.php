<?php namespace App\Services;

use App\Models;
use App\Contracts;
use App\Repositories;
/**
* 
*/
class ReporteCivilService implements Contracts\IReporteCivilService {
	private $_repoEntradas;
	private $_repoSalidas;
	private $_repoCatalogos;

	public function __construct(Contracts\IRepositorioCivil $entradas, Contracts\IRepositorioCivil $salidas, Repositories\RepositorioCatalogos $catalogos )
    {
        $this->_repoEntradas = $entradas;
        $this->_repoSalidas = $salidas;
        $this->_repoCatalogos = $catalogos;
    }

    public function createReporteJuzgadoByRubroDesgloseAnual($id_juzgado, $id_rubro, $anyo) {
    	$juzgado = $this->_repoCatalogos->getJuzgadoById($id_juzgado);
    	$rubro = $this->_repoCatalogos->getRubroJuicioById($id_rubro);
    	$table = $this->createTableDesglose();
    	$entradas = $this->_repoEntradas->getReporteJuzgadoByRubroDesgloseAnual($id_juzgado, $id_rubro, $anyo);
    	foreach ($entradas as $e) {
    		$table[$e->fila - 1]["Ingreso"] = $e->ingreso;
    		$table[$e->fila - 1]["Reingreso"] = $e->reingreso;
    		$table[$e->fila - 1]["Archivo Provisional (E)"] = $e->archivoprovisional;
    	}
    	$salidas = $this->_repoSalidas->getReporteJuzgadoByRubroDesgloseAnual($id_juzgado, $id_rubro, $anyo);
    	foreach ($salidas as $s) {
    		$table[$s->fila - 1]["Sentencia Procedente"] = $s->sprocedente;
    		$table[$s->fila - 1]["Sentencia Improcedente"] = $s->simprocedente;
    		$table[$s->fila - 1]["Auto"] = $s->auto;
    		$table[$s->fila - 1]["Excusa"] = $s->excusa;
    		$table[$s->fila - 1]["Recusacion"] = $s->recusacion;
    		$table[$s->fila - 1]["Acumulacion"] = $s->acumulacion;
    		$table[$s->fila - 1]["Archivo Provisional (S)"] = $s->archivoprovisional;
    	}
      $table[] = $this->createReporteJuzgadoByRubroAnual($id_juzgado, $id_rubro, $anyo)["filas"][0];
    	$titulo = "Rubro: ".$rubro->juicio->juicio.", ".$rubro->rubro;
      return $this->createJsonReporte($titulo, $table);
    }

    public function createReporteJuzgadoByRubroAnual($id_juzgado, $id_rubro, $anyo) {
        $juzgado = $this->_repoCatalogos->getJuzgadoById($id_juzgado);
        $rubro = $this->_repoCatalogos->getRubroJuicioById($id_rubro);
        $rows = array();
        $row = array("Etiqueta" => "Total " + $anyo
                       , "Ingreso" => 0
                       ,"Reingreso" => 0
                       ,"Archivo Provisional (E)" => 0
                       ,"Sentencia Procedente" => 0
                       ,"Sentencia Improcedente" => 0
                       ,"Auto" => 0
                       ,"Excusa" => 0
                       ,"Recusacion" => 0 
                       ,"Acumulacion" => 0
                       ,"Archivo Provisional (S)" => 0
                   );
         $entrada = $this->_repoEntradas->getReporteJuzgadoByRubroAnual($id_juzgado, $id_rubro, $anyo);
         if (!$entrada->isEmpty()){
            $row["Ingreso"] = $entrada[0]->ingreso;
            $row["Reingreso"] = $entrada[0]->reingreso;
            $row["Archivo Provisional (E)"] = $entrada[0]->archivoprovisional;
         }
         $salida = $this->_repoSalidas->getReporteJuzgadoByRubroAnual($id_juzgado, $id_rubro, $anyo);
         if (!$salida->isEmpty()){
            $row["Sentencia Procedente"] = $salida[0]->sprocedente;
            $row["Sentencia Improcedente"] = $salida[0]->simprocedente;
            $row["Auto"] = $salida[0]->auto;
            $row["Excusa"] = $salida[0]->excusa;
            $row["Recusacion"] = $salida[0]->recusacion;
            $row["Acumulacion"] = $salida[0]->acumulacion;
            $row["Archivo Provisional (S)"] = $salida[0]->archivoprovisional;
         }
         $rows[] = $row;

         $titulo = "Rubro: ".$rubro->juicio->juicio.", ".$rubro->rubro;
         return $this->createJsonReporte($titulo, $rows);
    }

    public function createReporteJuzgadoByJuicioDesgloseAnual($id_juzgado, $id_juicio, $anyo) {
        $juzgado = $this->_repoCatalogos->getJuzgadoById($id_juzgado);
        $juicio = $this->_repoCatalogos->getJuicioById($id_juicio);
        $table = $this->createTableDesglose();
        $entradas = $this->_repoEntradas->getReporteJuzgadoByJuicioDesgloseAnual($id_juzgado, $id_juicio, $anyo);
        foreach ($entradas as $e) {
            $table[$e->fila - 1]["Ingreso"] = $e->ingreso;
            $table[$e->fila - 1]["Reingreso"] = $e->reingreso;
            $table[$e->fila - 1]["Archivo Provisional (E)"] = $e->archivoprovisional;
        }
        $salidas = $this->_repoSalidas->getReporteJuzgadoByJuicioDesgloseAnual($id_juzgado, $id_juicio, $anyo);
        foreach ($salidas as $s) {
            $table[$s->fila - 1]["Sentencia Procedente"] = $s->sprocedente;
            $table[$s->fila - 1]["Sentencia Improcedente"] = $s->simprocedente;
            $table[$s->fila - 1]["Auto"] = $s->auto;
            $table[$s->fila - 1]["Excusa"] = $s->excusa;
            $table[$s->fila - 1]["Recusacion"] = $s->recusacion;
            $table[$s->fila - 1]["Acumulacion"] = $s->acumulacion;
            $table[$s->fila - 1]["Archivo Provisional (S)"] = $s->archivoprovisional;
        }
        $table[] = $this->createReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo)["filas"][0];
        
        $titulo = "Juicio: ".$juicio->juicio;
        return $this->createJsonReporte($titulo, $table);
    }

    public function createReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo) {
        $juzgado = $this->_repoCatalogos->getJuzgadoById($id_juzgado);
        $juicio = $this->_repoCatalogos->getJuicioById($id_juicio);
        $rows = array();
        $row = array("Etiqueta" => "Total " + $anyo
                       , "Ingreso" => 0
                       ,"Reingreso" => 0
                       ,"Archivo Provisional (E)" => 0
                       ,"Sentencia Procedente" => 0
                       ,"Sentencia Improcedente" => 0
                       ,"Auto" => 0
                       ,"Excusa" => 0
                       ,"Recusacion" => 0 
                       ,"Acumulacion" => 0
                       ,"Archivo Provisional (S)" => 0
                   );
         $entrada = $this->_repoEntradas->getReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo);
         if (!$entrada->isEmpty()){
            $row["Ingreso"] = $entrada[0]->ingreso;
            $row["Reingreso"] = $entrada[0]->reingreso;
            $row["Archivo Provisional (E)"] = $entrada[0]->archivoprovisional;
         }
         $salida = $this->_repoSalidas->getReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo);
         if (!$salida->isEmpty()){
            $row["Sentencia Procedente"] = $salida[0]->sprocedente;
            $row["Sentencia Improcedente"] = $salida[0]->simprocedente;
            $row["Auto"] = $salida[0]->auto;
            $row["Excusa"] = $salida[0]->excusa;
            $row["Recusacion"] = $salida[0]->recusacion;
            $row["Acumulacion"] = $salida[0]->acumulacion;
            $row["Archivo Provisional (S)"] = $salida[0]->archivoprovisional;
         }
         $rows[] = $row;
         
         $titulo = "Juicio: ".$juicio->juicio;
         return $this->createJsonReporte($titulo, $rows);
    }

    private function createTableDesglose() {
    	$meses = $this->_repoCatalogos->getMeses();
    	$rows = array();

    	foreach ($meses as $m) {
    		$row = array("Etiqueta" => $m
    			       , "Ingreso" => 0
    			       ,"Reingreso" => 0
    			       ,"Archivo Provisional (E)" => 0
    			       ,"Sentencia Procedente" => 0
                       ,"Sentencia Improcedente" => 0
                       ,"Auto" => 0
                       ,"Excusa" => 0
                       ,"Recusacion" => 0 
                       ,"Acumulacion" => 0
                       ,"Archivo Provisional (S)" => 0
    			   );
    	 	$rows[] = $row;
    	}

    	return $rows;
    }

    private function createJsonReporte($titulo, $filas)
    {
      $reporte = array("titulo" => $titulo, "filas" => $filas);
      return $reporte;
    }
}