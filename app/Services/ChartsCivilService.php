<?php namespace App\Services;

use App\ChartInfo;
use App\Models;
/**
* 
*/
class ChartsCivilService
{
        
    
    public function __construct()
    {
    
    }

    public function getGeneralTotal()
    {
        $labels = \App\Models\EntradaCivil :: distinct()
                 ->selectRaw("CAST(año as CHAR(4)) as year")
                 ->orderBy("año","desc")
                 //->get()
                 ->lists("year");
        $data = array();
        $total = \App\Models\EntradaCivil :: selectRaw("CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->groupBy("año")
                ->orderBy("año","desc")
                //->get();
                ->lists("total");
        $data[] = $total;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "General total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }


    public function getGeneralAnual($anyo){
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $data = array();
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $total = \App\Models\EntradaCivil :: selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($total as $t) {
            $ingresos[($t->mes)-1] = $t->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "General anual ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getPorJuzgadoTotal($juzgado)
    {
        $labels = \App\Models\EntradaCivil :: distinct()
                 ->selectRaw("CAST(año as CHAR(4)) as year")
                 ->where("id_juzgado","=", $juzgado->id_juzgado)
                 ->orderBy("año","desc")
                 //->get()
                 ->lists("year");
        $data = array();
        $total = \App\Models\EntradaCivil :: selectRaw("CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->groupBy("año")
                ->orderBy("año","desc")
                //->get();
                ->lists("total");
        $data[] = $total;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getPorJuzgadoAnual($juzgado, $anyo)
    {
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $data = array();
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $total = \App\Models\EntradaCivil :: selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($total as $t) {
            $ingresos[($t->mes)-1] = $t->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getDesgloseEntradasTotal()
    {
        $labels = \App\Models\EntradaCivil :: distinct()
                 ->selectRaw("CAST(año as CHAR(4)) as year")
                 ->orderBy("año","desc")
                 //->get()
                 ->lists("year");
        $entradas = \App\Models\EntradaCivil :: selectRaw("CAST(sum(ingreso) AS SIGNED) as ingresos, CAST(sum(reingreso) AS SIGNED) as reingresos, CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $data = array($entradas->lists("ingresos"),
                      $entradas->lists("reingresos"),
                      $entradas->lists("ap"));
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Desglose entradas";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getDesgloseEntradasAnual($anyo){
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $reingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: selectRaw("mes, 
                CAST(sum(ingreso) AS SIGNED) as ingresos, 
                CAST(sum(reingreso) AS SIGNED) as reingresos,
                CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->ingresos;
            $reingresos[($e->mes)-1] = $e->reingresos;
            $ap[($e->mes)-1] = $e->ap;
        }
        $data = array( $ingresos, $reingresos, $ap);
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas anual ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getDesgloseEntradasPorJuzgadoTotal($juzgado)
    {
        $labels = \App\Models\EntradaCivil :: distinct()
                 ->selectRaw("CAST(año as CHAR(4)) as year")
                 ->where("id_juzgado","=", $juzgado->id_juzgado)
                 ->orderBy("año","desc")
                 //->get()
                 ->lists("year");
        $entradas = \App\Models\EntradaCivil :: selectRaw("CAST(sum(ingreso) AS SIGNED) as ingresos,
                                                        CAST(sum(reingreso) AS SIGNED) as reingresos,
                                                        CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $data = array($entradas->lists("ingresos"),
                      $entradas->lists("reingresos"),
                      $entradas->lists("ap"));
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo."entradas total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getEntradasPorJuzgadoAnual($juzgado, $anyo)
    {
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $reingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: selectRaw("mes, 
                                 CAST(sum(ingreso) AS SIGNED) as ingresos, 
                                 CAST(sum(reingreso) AS SIGNED) as reingresos,
                                 CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->ingresos;
            $reingresos[($e->mes)-1] = $e->reingresos;
            $ap[($e->mes)-1] = $e->ap;
        }
        $data = array( $ingresos, $reingresos, $ap);
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getEntradasPorJuicioTotal($juicio){
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) use ($juicio){
                    $q->where("id_juicio","=",$juicio->id_juicio);
                })
                ->selectRaw("año, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("total"));
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas por juicio";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }

    public function getEntradasPorJuicioAnual($juicio, $anyo){
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) use ($juicio){
                    $q->where("id_juicio","=",$juicio->id_juicio);
                })
                ->selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas por juicio en el año ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }

    public function getDesgloseEntradasGeneralPorJuicioTotal($juicio)
    {
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) use ($juicio){
                    $q->where("id_juicio","=",$juicio->id_juicio);
                })
                ->selectRaw("año, CAST(sum(ingreso) AS SIGNED) as ingresos, CAST(sum(reingreso) AS SIGNED) as reingresos, CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("ingresos"),
                      $entradas->lists("reingresos"),
                      $entradas->lists("ap"));
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Desglose entradas";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getDesgloseEntradasGeneralPorJuicioAnual($juicio, $anyo){
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $reingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) use ($juicio){
                    $q->where("id_juicio","=",$juicio->id_juicio);
                })
                ->selectRaw("mes, 
                CAST(sum(ingreso) AS SIGNED) as ingresos, 
                CAST(sum(reingreso) AS SIGNED) as reingresos,
                CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->ingresos;
            $reingresos[($e->mes)-1] = $e->reingresos;
            $ap[($e->mes)-1] = $e->ap;
        }
        $data = array( $ingresos, $reingresos, $ap);
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas anual ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getEntradasJuzgadoPorJuicioTotal($juzgado, $juicio){
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) use ($juicio){
                    $q->where("id_juicio","=",$juicio->id_juicio);
                })
                ->selectRaw("año, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("total"));
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas en ".$juzgado->prefijo." por juicio";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }

    public function getEntradasJuzgadoPorJuicioAnual($juzgado, $juicio, $anyo){
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) use ($juicio){
                    $q->where("id_juicio","=",$juicio->id_juicio);
                })
                ->selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas en ".$juzgado->prefijo ." por juicio en el año ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }

    public function getDesgloseEntradasJuzgadoPorJuicioTotal($juzgado, $juicio)
    {
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) use ($juicio){
                    $q->where("id_juicio","=",$juicio->id_juicio);
                })
                ->selectRaw("año, CAST(sum(ingreso) AS SIGNED) as ingresos,
                                                        CAST(sum(reingreso) AS SIGNED) as reingresos,
                                                        CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("ingresos"),
                      $entradas->lists("reingresos"),
                      $entradas->lists("ap"));
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo."entradas total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getDesgloseEntradasJuzgadoPorJuicioAnual($juzgado, $juicio, $anyo)
    {
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $reingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: whereHas('RubroJuicio', function($q) use ($juicio){
                    $q->where("id_juicio","=",$juicio->id_juicio);
                })
                ->selectRaw("mes, 
                                 CAST(sum(ingreso) AS SIGNED) as ingresos, 
                                 CAST(sum(reingreso) AS SIGNED) as reingresos,
                                 CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->ingresos;
            $reingresos[($e->mes)-1] = $e->reingresos;
            $ap[($e->mes)-1] = $e->ap;
        }
        $data = array( $ingresos, $reingresos, $ap);
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }


    public function getGeneralPorRubroTotal($rubro){
        $entradas = \App\Models\EntradaCivil :: selectRaw("año, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_rubroJuicio","=",$rubro->id_rubroJuicio)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("total"));
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas por rubro";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }

    public function getGeneralPorRubroAnual($rubro, $anyo){
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("año","=",$anyo)
                ->where("id_rubroJuicio","=",$rubro->id_rubroJuicio)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas por juicio en el año ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }

    public function getDesgloseEntradasGeneralPorRubroTotal($rubro)
    {
        $entradas = \App\Models\EntradaCivil :: selectRaw("año, CAST(sum(ingreso) AS SIGNED) as ingresos, CAST(sum(reingreso) AS SIGNED) as reingresos, CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->where("id_rubroJuicio","=",$rubro->id_rubroJuicio)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("ingresos"),
                      $entradas->lists("reingresos"),
                      $entradas->lists("ap"));
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Desglose entradas";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getDesgloseEntradasGeneralPorRubroAnual($rubro, $anyo){
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $reingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: selectRaw("mes, 
                CAST(sum(ingreso) AS SIGNED) as ingresos, 
                CAST(sum(reingreso) AS SIGNED) as reingresos,
                CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("año","=",$anyo)
                ->where("id_rubroJuicio","=",$rubro->id_rubroJuicio)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->ingresos;
            $reingresos[($e->mes)-1] = $e->reingresos;
            $ap[($e->mes)-1] = $e->ap;
        }
        $data = array( $ingresos, $reingresos, $ap);
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas anual ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getJuzgadoPorRubroTotal($juzgado, $rubro){
        $entradas = \App\Models\EntradaCivil :: selectRaw("año, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("id_rubroJuicio","=",$rubro->id_rubroJuicio)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("total"));
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas en ".$rubro->rubro." por juicio";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }

    public function getJuzgadoPorRubroAnual($juzgado, $rubro, $anyo){
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: selectRaw("mes, CAST(sum(ingreso)+sum(reingreso)+sum(archivoprovisional) as SIGNED )as total")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                ->where("id_rubroJuicio","=",$rubro->id_rubroJuicio)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->total;
        }
        $data[] = $ingresos;
        $series = array("Entradas");
        
        $chart = new ChartInfo();

        $chart->titulo = "Entradas en ".$juzgado->prefijo ." por rubro en el año ".$anyo;
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;   
    }

    public function getDesgloseEntradasJuzgadoPorRubroTotal($juzgado, $rubro)
    {
        $entradas = \App\Models\EntradaCivil :: selectRaw("año, CAST(sum(ingreso) AS SIGNED) as ingresos,
                                                        CAST(sum(reingreso) AS SIGNED) as reingresos,
                                                        CAST(sum(archivoprovisional) AS SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                >where("id_rubroJuicio","=",$rubro->id_rubroJuicio)
                ->groupBy("año")
                ->orderBy("año","desc")
                ->get();
        $labels = $entradas->lists("año");
        $data = array($entradas->lists("ingresos"),
                      $entradas->lists("reingresos"),
                      $entradas->lists("ap"));
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo."entradas total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

    public function getDesgloseEntradasJuzgadoPorRubroAnual($juzgado, $rubro, $anyo)
    {
        $labels = array("Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        
        $ingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $reingresos = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $ap = array(0,0,0,0,0,0,0,0,0,0,0,0);
        $entradas = \App\Models\EntradaCivil :: selectRaw("mes, 
                                 CAST(sum(ingreso) AS SIGNED) as ingresos, 
                                 CAST(sum(reingreso) AS SIGNED) as reingresos,
                                 CAST(sum(archivoprovisional) as SIGNED) as ap")
                ->where("id_juzgado","=", $juzgado->id_juzgado)
                >where("id_rubroJuicio","=",$rubro->id_rubroJuicio)
                ->where("año","=",$anyo)
                ->groupBy("mes")
                ->orderBy("mes","desc")
                ->get();
        foreach ($entradas as $e) {
            $ingresos[($e->mes)-1] = $e->ingresos;
            $reingresos[($e->mes)-1] = $e->reingresos;
            $ap[($e->mes)-1] = $e->ap;
        }
        $data = array( $ingresos, $reingresos, $ap);
        $series = array("Ingresos", "Reingresos", "Archivo Provisional");
        
        $chart = new ChartInfo();

        $chart->titulo = $juzgado->prefijo." total";
        $chart->etiquetas = $labels;
        $chart->series = $series;
        $chart->data = $data;

        return $chart;
    }

}