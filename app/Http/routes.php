<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Services\ChartsCivilService;
use App\Services;
use App\ChartScopes;
use App\Repositories;


Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('juzgados', 'JuzgadosController@index');
Route::get('juzgados/{id}', 'JuzgadosController@getDetalle');
Route::get('juzgados/{id}/juicios', function($id){
	$juzgado = \App\Models\Juzgado :: find($id);
	return $juzgado->tipojuzgado->getJuicios();
});

Route::get('catalogos/juicios','CatalogosController@getJuicios');
Route::get('catalogos/juicios/{id}','CatalogosController@getDetalleJuicio');
Route::get('catalogos/juicios/{id}/rubros', function ($id){
	$juicio = \App\Models\Juicio :: find($id);
	return $juicio->rubroJuicios;
});
Route::get('catalogos/tipojuzgados/{id}','CatalogosController@getDetalleTipoJuzgado' );
Route::get('catalogos/tipojuzgados/{id}/juicios', function($id){
	$tipo = \App\Models\TipoJuzgado :: find($id);
	return $tipo->getJuicios();
});
//Route::get('catalogos/reportes','CatalogosController@getReportes');
Route::get('catalogos/materias','CatalogosController@getMaterias');
Route::get('catalogos/tipojuzgados','CatalogosController@getTipoJuzgados');

Route::post('catalogos/juicios','CatalogosController@postJuicio' );
//Route::post('catalogos/reportes','CatalogosController@postReporte' );
Route::post('catalogos/materias','CatalogosController@postMateria' );
Route::post('catalogos/tipojuzgados','CatalogosController@postTipoJuzgado' );
Route::post('catalogos/tipojuzgados/{id}','CatalogosController@updateTipoJuzgado' );

Route::post('catalogos/juicios/{id}',function(Request $request,$id){
	$rubro = new \App\Models\RubroJuicio;
	$rubro->id_rubroJuicio = $request->input('id_rubroJuicio');
	$rubro->id_juicio = $id;
	$rubro->rubro = $request->input('rubro');
	$rubro->save();
	return $rubro;
});

Route::get('captura/civil/entrada','CapturaController@getCapturaEntradaCivil');
Route::post('captura/civil/entrada',function (Request $request){
	$r = new \App\PostResponse;
	
	$match = ['id_juzgado' => $request->input('juzgado')
			,'id_rubroJuicio' => $request->input('rubro')
			,'año' => $request->input('anyo')
			,'mes' => $request->input('mes') 
	];
	$rowCount = \App\Models\EntradaCivil::where($match)->count();

	if($rowCount == 0)
	{
		$entrada = new \App\Models\EntradaCivil;
		$entrada->id_juzgado = $request->input('juzgado');
		$entrada->id_rubroJuicio = $request->input('rubro');
		$entrada->año = $request->input('anyo');
		$entrada->mes = $request->input('mes');
		$entrada->ingreso = $request->input('ingreso');
		$entrada->reingreso = $request->input('reingreso');
		$entrada->archivoprovisional = $request->input('ap');
		$entrada->save();
		$r->status = "OK";
		$r->mensaje = "Registrado con id: ".$entrada->id_entradas_civil;
	}
	else
	{
		$r->status = "WARNING";
		$r->mensaje = "Ya existen datos capturados";
	}
	return response()->json($r);
});
Route::get('captura/civil/salida','CapturaController@getCapturaSalidaCivil');
Route::post('captura/civil/salida',function (Request $request){
	$r = new \App\PostResponse;
	
	$match = ['id_juzgado' => $request->input('juzgado')
			,'id_rubroJuicio' => $request->input('rubro')
			,'año' => $request->input('anyo')
			,'mes' => $request->input('mes') 
	];
	$rowCount = \App\Models\SalidaCivil::where($match)->count();

	if($rowCount == 0)
	{
		$salida = new \App\Models\SalidaCivil;
		$salida->id_juzgado = $request->input('juzgado');
		$salida->id_rubroJuicio = $request->input('rubro');
		$salida->año = $request->input('anyo');
		$salida->mes = $request->input('mes');
		$salida->sprocedente = $request->input('procedente');
		$salida->simprocedente = $request->input('improcedente');
		$salida->auto = $request->input('auto');
		$salida->excusa = $request->input('excusa');
		$salida->recusacion = $request->input('recusacion');
		$salida->acumulacion = $request->input('acumulacion');
		$salida->archivoprovisional = $request->input('ap');
		$salida->save();
		$r->status = "OK";
		$r->mensaje = "Registrado con id: ".$salida->id_salidas_civil;
	}
	else
	{
		$r->status = "WARNING";
		$r->mensaje = "Ya existen datos capturados";
	}
	return response()->json($r);
});

Route::get('captura/familiar/entrada','CapturaController@getCapturaEntradaFamiliar');
Route::get('captura/familiar/salida','CapturaController@getCapturaSalidaFamiliar');
Route::post('captura/familiar/entrada','CapturaController@postCapturaEntradaFamiliar');
Route::post('captura/familiar/salida','CapturaController@postCapturaSalidaFamiliar');

Route::get('desglose/civil/entrada','DesgloseController@showDesgloseEntradaCivil');
Route::get('desglose/civil/salida','DesgloseController@showDesgloseSalidaCivil');
Route::get('desglose/civil/entrada/detalle','DesgloseController@getDesgloseEntradaCivil');
Route::get('desglose/civil/salida/detalle','DesgloseController@getDesgloseSalidaCivil');
Route::post('desglose/civil/entrada/detalle','DesgloseController@updateEntradaCivil');
Route::post('desglose/civil/salida/detalle','DesgloseController@updateSalidaCivil');

Route::get('desglose/familiar/entrada','DesgloseFamiliarController@showDesgloseEntrada');
Route::get('desglose/familiar/salida','DesgloseFamiliarController@showDesgloseSalida');
Route::get('desglose/familiar/entrada/detalle','DesgloseFamiliarController@getDesgloseEntrada');
Route::get('desglose/familiar/salida/detalle','DesgloseFamiliarController@getDesgloseSalida');
Route::post('desglose/familiar/entrada/detalle','DesgloseFamiliarController@updateEntrada');
Route::post('desglose/familiar/salida/detalle','DesgloseFamiliarController@updateSalida');

Route::get('informe/civil', function (){
	return redirect('informe/civil/global');
});
Route::get('informe/civil/global','InformesController@getInformeCivilGlobal');
Route::get('informe/civil/global/{anyo}','InformesController@getInformeCivilGlobalAnual');

Route::get('informe/civil/juzgado/{prefix}','InformesController@getInformeCivilGlobalPorJuzgado');
Route::get('informe/civil/juzgado/{prefijo}/{anyo}','InformesController@getInformeCivilPorJuzgadoAnual');


Route::get('informe/familiar', function (){
	return redirect('informe/familiar/global');
});
Route::get('informe/familiar/global','InformesFamiliarController@getInformeGlobal');
Route::get('informe/familiar/global/{anyo}','InformesFamiliarController@getInformeGlobalAnual');

Route::get('informe/familiar/juzgado/{prefix}','InformesFamiliarController@getInformeGlobalPorJuzgado');
Route::get('informe/familiar/juzgado/{prefijo}/{anyo}','InformesFamiliarController@getInformePorJuzgadoAnual');


Route::get('reporte/civil', function(){

		$juzgados = \App\Models\Juzgado :: where('tipo','<>','P')->where('tipo','<>','F')->get();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('informes\reportecivil',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	
});

Route::get('reporte/civil/generar', function(Request $request){
	$id_juzgado = $request->input('juzgado');
	$anyo = $request->input('anyo');
	$id_juicio = $request->input('juicio');
	$id_rubro = $request->input('rubro');
	$desglosado = $request->input("desglosado");
	$srvReporte = new Services\ReporteCivilService(new Repositories\RepositorioEntradasCivil()
													, new Repositories\RepositorioSalidasCivil()
													, new Repositories\RepositorioCatalogos()
												);
	$data = null;
	if($id_juicio > 0 && $id_rubro == 0){
		if($desglosado == 1) {
			$data =	$srvReporte->createReporteJuzgadoByJuicioDesgloseAnual($id_juzgado, $id_juicio, $anyo);
		}
		else {
			$data =	$srvReporte->createReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo);
		}
	}
	else if($id_rubro > 0){
		
		if($desglosado == 1) {
			$data =	$srvReporte->createReporteJuzgadoByRubroDesgloseAnual($id_juzgado, $id_rubro, $anyo);
		}
		else {
			$data =	$srvReporte->createReporteJuzgadoByRubroAnual($id_juzgado, $id_rubro, $anyo);	
		}
	}
	return response()->json($data);
});

Route::get('reporte/familiar', function(){

		$juzgados = \App\Models\Juzgado :: where('id_tipojuzgado',4)->get();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('informes\reportefamiliar',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	
});

Route::get('reporte/familiar/generar', function(Request $request){
	$id_juzgado = $request->input('juzgado');
	$anyo = $request->input('anyo');
	$id_juicio = $request->input('juicio');
	$id_rubro = $request->input('rubro');
	$desglosado = $request->input("desglosado");
	$srvReporte = new Services\ReporteFamiliarService(new Repositories\RepositorioEntradasFamiliar()
													, new Repositories\RepositorioSalidasFamiliar()
													, new Repositories\RepositorioCatalogos()
												);
	$data = null;
	if($id_juicio > 0 && $id_rubro == 0){
		if($desglosado == 1) {
			$data =	$srvReporte->createReporteJuzgadoByJuicioDesgloseAnual($id_juzgado, $id_juicio, $anyo);
		}
		else {
			$data =	$srvReporte->createReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo);
		}
	}
	else if($id_rubro > 0){
		
		if($desglosado == 1) {
			$data =	$srvReporte->createReporteJuzgadoByRubroDesgloseAnual($id_juzgado, $id_rubro, $anyo);
		}
		else {
			$data =	$srvReporte->createReporteJuzgadoByRubroAnual($id_juzgado, $id_rubro, $anyo);	
		}
	}
	return response()->json($data);
});

Route::get('charts/civil/global','ChartsGeneralController@getTotal');
Route::get('charts/civil/global/{anyo}','ChartsGeneralController@getAnual');
Route::get('charts/civil/juzgado/{prefijo}', 'ChartsGeneralController@getByJuzgadoTotal');
Route::get('charts/civil/juzgado/{prefijo}/{anyo}', 'ChartsGeneralController@getByJuzgadoAnual' );

Route::get('charts/civil/entradas/global','ChartsEntradasController@getTotal');
Route::get('charts/civil/entradas/global/{anyo}','ChartsEntradasController@getAnual');
Route::get('charts/civil/entradas/juzgado/{prefijo}', 'ChartsEntradasController@getByJuzgadoTotal');
Route::get('charts/civil/entradas/juzgado/{prefijo}/{anyo}', 'ChartsEntradasController@getByJuzgadoAnual' );

Route::get('charts/civil/salidas/global','ChartsSalidasController@getTotal');
Route::get('charts/civil/salidas/global/{anyo}','ChartsSalidasController@getAnual');
Route::get('charts/civil/salidas/juzgado/{prefijo}', 'ChartsSalidasController@getByJuzgadoTotal');
Route::get('charts/civil/salidas/juzgado/{prefijo}/{anyo}', 'ChartsSalidasController@getByJuzgadoAnual' );

Route::get('charts/familiar/global','ChartsGeneralFamiliarController@getTotal');
Route::get('charts/familiar/global/{anyo}','ChartsGeneralFamiliarController@getAnual');
Route::get('charts/familiar/juzgado/{prefijo}', 'ChartsGeneralFamiliarController@getByJuzgadoTotal');
Route::get('charts/familiar/juzgado/{prefijo}/{anyo}', 'ChartsGeneralFamiliarController@getByJuzgadoAnual' );

Route::get('charts/familiar/entradas/global','ChartsEntradasFamiliarController@getTotal');
Route::get('charts/familiar/entradas/global/{anyo}','ChartsEntradasFamiliarController@getAnual');
Route::get('charts/familiar/entradas/juzgado/{prefijo}', 'ChartsEntradasFamiliarController@getByJuzgadoTotal');
Route::get('charts/familiar/entradas/juzgado/{prefijo}/{anyo}', 'ChartsEntradasFamiliarController@getByJuzgadoAnual' );

Route::get('charts/familiar/salidas/global','ChartsSalidasFamiliarController@getTotal');
Route::get('charts/familiar/salidas/global/{anyo}','ChartsSalidasFamiliarController@getAnual');
Route::get('charts/familiar/salidas/juzgado/{prefijo}', 'ChartsSalidasFamiliarController@getByJuzgadoTotal');
Route::get('charts/familiar/salidas/juzgado/{prefijo}/{anyo}', 'ChartsSalidasFamiliarController@getByJuzgadoAnual' );

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
