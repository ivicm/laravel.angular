<?php namespace App\Http\Controllers;

use App\Models;
use App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CapturaController extends Controller {


	private $_repoCatalogos;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
		$this->_repoCatalogos = new Repositories\RepositorioCatalogos();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getCapturaEntradaCivil()
	{
		//$juzgados = \App\Models\Juzgado :: where('tipo','<>','P')->get();
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaCivil();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('captura\entradacivil',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	}

	public function getCapturaSalidaCivil()
	{
		//$juzgados = \App\Models\Juzgado :: where('tipo','<>','P')->get();
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaCivil();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('captura\salidacivil',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	}

	public function getCapturaEntradaFamiliar()
	{
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaFamiliar();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('captura\entradafamiliar',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	}

	public function getCapturaSalidaFamiliar()
	{
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaFamiliar();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('captura\salidafamiliar',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	}

	public function postCapturaEntradaFamiliar(Request $request)
	{
		$r = new \App\PostResponse;
		
		$match = ['id_juzgado' => $request->input('juzgado')
				,'id_rubroJuicio' => $request->input('rubro')
				,'año' => $request->input('anyo')
				,'mes' => $request->input('mes') 
		];
		$rowCount = \App\Models\EntradaFamiliar::where($match)->count();

		if($rowCount == 0)
		{
			$entrada = new \App\Models\EntradaFamiliar;
			$entrada->id_juzgado = $request->input('juzgado');
			$entrada->id_rubroJuicio = $request->input('rubro');
			$entrada->año = $request->input('anyo');
			$entrada->mes = $request->input('mes');
			$entrada->ingreso = $request->input('ingreso');
			$entrada->reingreso = $request->input('reingreso');
			$entrada->archivoprovisional = $request->input('ap');
			$entrada->save();
			$r->status = "OK";
			$r->mensaje = "Registrado con id: ".$entrada->id_entradas_familiar;
		}
		else
		{
			$r->status = "WARNING";
			$r->mensaje = "Ya existen datos capturados";
		}
		return response()->json($r);
	}

	public function postCapturaSalidaFamiliar (Request $request)
	{
		$r = new \App\PostResponse;
		
		$match = ['id_juzgado' => $request->input('juzgado')
				,'id_rubroJuicio' => $request->input('rubro')
				,'año' => $request->input('anyo')
				,'mes' => $request->input('mes') 
		];
		$rowCount = \App\Models\SalidaFamiliar::where($match)->count();

		if($rowCount == 0)
		{
			$salida = new \App\Models\SalidaFamiliar;
			$salida->id_juzgado = $request->input('juzgado');
			$salida->id_rubroJuicio = $request->input('rubro');
			$salida->año = $request->input('anyo');
			$salida->mes = $request->input('mes');
			$salida->sprocedente = $request->input('procedente');
			$salida->simprocedente = $request->input('improcedente');
			$salida->auto = $request->input('auto');
			$salida->excusarecusacion = $request->input('excusarecusacion');
			$salida->competencia = $request->input('competencia');
			$salida->acumulacion = $request->input('acumulacion');
			$salida->archivoprovisional = $request->input('ap');
			$salida->save();
			$r->status = "OK";
			$r->mensaje = "Registrado con id: ".$salida->id_salidas_familiar;
		}
		else
		{
			$r->status = "WARNING";
			$r->mensaje = "Ya existen datos capturados";
		}
		return response()->json($r);
	}
}