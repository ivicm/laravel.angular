<?php namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\ChartsCivilService;
use App\Services;
use App\Contracts;
use App\ChartScopes;

class ChartsGeneralFamiliarController extends Controller {

	
	private $_scope;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
		//$this->middleware('auth');
		if($request->has("juicio"))
		{
			$id_juicio = $request->input("juicio");
			$juicio = \App\Models\Juicio :: find($id_juicio);
			//$this->_scope = new ChartScopes\JuicioGeneralChart($juicio);
			$this->_scope = new ChartScopes\GeneralFamiliarChart(new ChartScopes\JuicioEntradasFamiliarChart($juicio)
				,new ChartScopes\JuicioSalidasFamiliarChart($juicio));
		}
		else if($request->has("rubro")){
			$id_rubro = $request->input("rubro");
			$rubro = \App\Models\RubroJuicio :: find($id_rubro);
			//$this->_scope = new ChartScopes\RubroGeneralChart($rubro);
			$this->_scope = new ChartScopes\GeneralFamiliarChart(new ChartScopes\RubroEntradasFamiliarChart($rubro)
				,new ChartScopes\RubroSalidasFamiliarChart($rubro));
		}
		else {
			//$this->_scope = new ChartScopes\GlobalGeneralChart();	
			$this->_scope = new ChartScopes\GeneralFamiliarChart(new ChartScopes\GlobalEntradasFamiliarChart()
				,new ChartScopes\GlobalSalidasFamiliarChart());
		}
		
	}

	public function getTotal(){
		$chart = $this->_scope->getTotal();
		return response()->json($chart);
	}

	public function getAnual($anyo){
		$chart = $this->_scope->getAnual($anyo);
		return response()->json($chart);
	}

    public function getByJuzgadoTotal($prefijo){
    	$juzgado = $this->getJuzgadoByPrefijo($prefijo);
		$chart = $this->_scope->getByJuzgadoTotal($juzgado);
		return response()->json($chart);
	}

	public function getByJuzgadoAnual($prefijo, $anyo){
		$juzgado = $this->getJuzgadoByPrefijo($prefijo);	
		$chart = $this->_scope->getByJuzgadoAnual($juzgado, $anyo);
		return response()->json($chart);
	}

	private function getJuzgadoByPrefijo($prefijo){
		$juzgado = \App\Models\Juzgado :: where('prefijo',$prefijo) 
	                ->where('tipo','<>','P')
	                ->first();
	    return $juzgado;	
	}
}