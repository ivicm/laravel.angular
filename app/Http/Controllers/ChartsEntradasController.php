<?php namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;
use App\Services\ChartsCivilService;
use App\Services;
use App\ChartScopes;

class ChartsEntradasController extends Controller {

	
	private $_scope;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
		//$this->middleware('auth');
		if($request->has("juicio"))
		{
			$id_juicio = $request->input("juicio");
			$juicio = \App\Models\Juicio :: find($id_juicio);
			$this->_scope = new ChartScopes\JuicioDesgloseEntradasChart($juicio);
		}
		else if($request->has("rubro")){
			$id_rubro = $request->input("rubro");
			$rubro = \App\Models\RubroJuicio :: find($id_rubro);
			$this->_scope = new ChartScopes\RubroDesgloseEntradasChart($rubro);
		}
		else if($request->has("materia")) {
			$id_materia = $request->input("materia");
			$materia = \App\Models\Materia :: find($id_materia);
			$this->_scope = new ChartScopes\MateriaDesgloseEntradasChart($materia);
		}
		else {
			$this->_scope = new ChartScopes\GlobalDesgloseEntradasChart();	
		}
		
	}

	public function getTotal(){
		$chart = $this->_scope->getTotal();
		return response()->json($chart);
	}

	public function getAnual($anyo){
		$chart = $this->_scope->getAnual($anyo);
		return response()->json($chart);
	}

    public function getByJuzgadoTotal($prefijo){
    	$juzgado = $this->getJuzgadoByPrefijo($prefijo);
		$chart = $this->_scope->getByJuzgadoTotal($juzgado);
		return response()->json($chart);
	}

	public function getByJuzgadoAnual($prefijo, $anyo){
		$juzgado = $this->getJuzgadoByPrefijo($prefijo);	
		$chart = $this->_scope->getByJuzgadoAnual($juzgado, $anyo);
		return response()->json($chart);
	}

	private function getJuzgadoByPrefijo($prefijo){
		$juzgado = \App\Models\Juzgado :: where('prefijo',$prefijo) 
	                ->where('tipo','<>','P')
	                ->first();
	    return $juzgado;	
	}
}