<?php namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories;

class DesgloseFamiliarController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	private $_repoCatalogos;

	public function __construct()
	{
		//$this->middleware('auth');
		$this->_repoCatalogos = new Repositories\RepositorioCatalogos();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function showDesgloseEntrada()
	{
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaFamiliar();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('desglose\entradafamiliar',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	}

	public function showDesgloseSalida()
	{
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaFamiliar();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('desglose\salidafamiliar',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	}

	public function getDesgloseEntrada(Request $request)
	{
		$response = Models\EntradaFamiliar :: selectRaw("id_entradas_familiar,mes, ingreso, reingreso, archivoprovisional")
		                                 ->where('id_juzgado',$request->input('juzgado'))
		                                 ->where('año',$request->input('anyo'))
		                                 ->where('id_rubrojuicio',$request->input('rubro'))
		                                 ->get();
		return response()->json($response);
	}

	public function getDesgloseSalida(Request $request)
	{
		$response = Models\SalidaFamiliar :: selectRaw("id_salidas_familiar,mes, sprocedente, simprocedente, 
			                                             auto, excusarecusacion, competencia,
			                                             acumulacion, archivoprovisional")
		                                 ->where('id_juzgado',$request->input('juzgado'))
		                                 ->where('año',$request->input('anyo'))
		                                 ->where('id_rubrojuicio',$request->input('rubro'))
		                                 ->get();
		return response()->json($response);
	}

	public function updateEntrada(Request $request)
	{
		$r = new \App\PostResponse;
		if($request->has('id'))
		{
			$entrada = Models\EntradaFamiliar :: find($request->input('id'));
			$entrada->ingreso = $request->input('ingreso');
			$entrada->reingreso = $request->input('reingreso');
			$entrada->archivoprovisional = $request->input('ap');
			$entrada->save();
			$r->status = "OK";
			$r->mensaje = "Información actualizada:";
		}
		else
		{
			$r->status = "ERROR";
			$r->mensaje = "No se encontró el registro";
		}
		return response()->json($r);
	}

	public function updateSalida(Request $request)
	{
		$r = new \App\PostResponse;
		if($request->has('id'))
		{
			$salida = Models\SalidaFamiliar :: find($request->input('id'));
			$salida->sprocedente = $request->input('sprocedente');
			$salida->simprocedente = $request->input('simprocedente');
			$salida->auto = $request->input('auto');
			$salida->excusarecusacion = $request->input('excusarecusacion');
			$salida->competencia = $request->input('competencia');
			$salida->acumulacion = $request->input('acumulacion');
			$salida->archivoprovisional = $request->input('ap');
			$salida->save();
			$r->status = "OK";
			$r->mensaje = "Información actualizada:";
		}
		else
		{
			$r->status = "ERROR";
			$r->mensaje = "No se encontró el registro";
		}
		return response()->json($r);
	}
}