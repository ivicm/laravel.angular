<?php namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DesgloseController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function showDesgloseEntradaCivil()
	{
		$juzgados = \App\Models\Juzgado :: where('tipo','<>','P')->get();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('desglose\entradacivil',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	}

	public function showDesgloseSalidaCivil()
	{
		$juzgados = \App\Models\Juzgado :: where('tipo','<>','P')->get();
		$juicios = $juzgados->first()->tipojuzgado->getJuicios();
		$rubros = \App\Models\RubroJuicio :: where('id_juicio','=', $juicios->first()->id_juicio)->get();
		return view('desglose\salidacivil',['juzgados' => $juzgados, 'juicios' => $juicios, 'rubros' => $rubros]);
	}

	public function getDesgloseEntradaCivil(Request $request)
	{
		$response = Models\EntradaCivil :: selectRaw("id_entradas_civil,mes, ingreso, reingreso, archivoprovisional")
		                                 ->where('id_juzgado',$request->input('juzgado'))
		                                 ->where('año',$request->input('anyo'))
		                                 ->where('id_rubrojuicio',$request->input('rubro'))
		                                 ->get();
		return response()->json($response);
	}

	public function getDesgloseSalidaCivil(Request $request)
	{
		$response = Models\SalidaCivil :: selectRaw("id_salidas_civil,mes, sprocedente, simprocedente, 
			                                             auto, excusa, recusacion,
			                                             acumulacion, archivoprovisional")
		                                 ->where('id_juzgado',$request->input('juzgado'))
		                                 ->where('año',$request->input('anyo'))
		                                 ->where('id_rubrojuicio',$request->input('rubro'))
		                                 ->get();
		return response()->json($response);
	}

	public function updateEntradaCivil(Request $request)
	{
		$r = new \App\PostResponse;
		if($request->has('id'))
		{
			$entrada = Models\EntradaCivil :: find($request->input('id'));
			$entrada->ingreso = $request->input('ingreso');
			$entrada->reingreso = $request->input('reingreso');
			$entrada->archivoprovisional = $request->input('ap');
			$entrada->save();
			$r->status = "OK";
			$r->mensaje = "Información actualizada:";
		}
		else
		{
			$r->status = "ERROR";
			$r->mensaje = "No se encontró el registro";
		}
		return response()->json($r);
	}

	public function updateSalidaCivil(Request $request)
	{
		$r = new \App\PostResponse;
		if($request->has('id'))
		{
			$salida = Models\SalidaCivil :: find($request->input('id'));
			$salida->sprocedente = $request->input('sprocedente');
			$salida->simprocedente = $request->input('simprocedente');
			$salida->auto = $request->input('auto');
			$salida->excusa = $request->input('excusa');
			$salida->recusacion = $request->input('recusacion');
			$salida->acumulacion = $request->input('acumulacion');
			$salida->archivoprovisional = $request->input('ap');
			$salida->save();
			$r->status = "OK";
			$r->mensaje = "Información actualizada:";
		}
		else
		{
			$r->status = "ERROR";
			$r->mensaje = "No se encontró el registro";
		}
		return response()->json($r);
	}
}