<?php namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;

class CatalogosController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getJuicios()
	{
		$juicios = \App\Models\Juicio :: all();
		$materias = \App\Models\Materia :: all();
		return view('catalogos\juicios',['juicios' => $juicios, 'materias' => $materias]);
	}

	/*public function getReportes()
	{
		$reportes = \App\Models\Reporte :: all();
		return view('catalogos\reportes',['reportes' => $reportes]);
	}*/

	public function getMaterias()
	{
		$materias = \App\Models\Materia :: all();
		return view('catalogos\materias',['materias' => $materias]);
	}

	public function getTipoJuzgados()
	{
		$tipos = \App\Models\TipoJuzgado :: all();
		$materias = \App\Models\Materia :: all();
		return view('catalogos\tipojuzgados',['tipos' => $tipos, 'materias' => $materias]);
	}

	public function getDetalleJuicio($id)
	{
		$juicio = \App\Models\Juicio :: find($id);
		return view('catalogos\detallejuicio',['juicio' => $juicio]);
	}

	public function getDetalleTipoJuzgado($id)
	{
		$tipo = \App\Models\TipoJuzgado::find($id);
		$materias = \App\Models\Materia::all();
		$checks = array();
		foreach ($materias as $m) {
			$c = New \App\ResponseCheckbox;
			$c->value = $m->id_materia;
			$c->label = $m->materia;
			$c->checked = "";
			foreach ($tipo->materias as $tm) {
				if($tm->id_materia == $m->id_materia)
				{
					$c->checked = "checked";
				}
			}
			$checks[] = $c;
		}
		return view('catalogos\detalletipojuzgado',['tipojuzgado'=> $tipo, 'checks' => $checks]);
	}

	public function postJuicio(Request $request)
	{
		if($request->has('id'))
		{
			$juicio = \App\Models\Juicio :: find($request->input('id'));
			$juicio->juicio = $request->input('juicio');
			$juicio->id_materia = $request->input('tipo');
			$juicio->save();
		}
		else {
			
			$juicio = new \App\Models\Juicio;
			$juicio->juicio = $request->input('juicio');
			$juicio->id_materia = $request->input('tipo');
			$juicio->save();
		}
		return $this->getJuicios();
	}

	/*public function postReporte(Request $request)
	{
		$reporte = new \App\Models\Reporte;
		$reporte->reporte = $request->input('reporte');
		$reporte->save();

		return $this->getReportes();
	}*/

	public function postMateria(Request $request)
	{
		$materia = new \App\Models\Materia;
		$materia->materia = $request->input('materia');
		$materia->save();

		return $this->getMaterias();
	}

	public function postTipoJuzgado(Request $request)
	{
		$tipo = new \App\Models\TipoJuzgado;
		$tipo->tipo = $request->input('tipo');
		$tipo->descripcion = $request->input('descripcion');
		$tipo->save();
		
		return $this->getTipoJuzgados();
	}

	public function updateTipoJuzgado(Request $request, $id)
	{
		$tipo = \App\Models\TipoJuzgado::find($id);
		$tipo->tipo = $request->input('tipo');
		$tipo->descripcion = $request->input('descripcion');
		$tipo->save();
		$tipo->materias()->sync($request->input('materias'));

		return redirect('catalogos/tipojuzgados')->with('message', 'Tipo de juzgado actualizado');
	}
}