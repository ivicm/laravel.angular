<?php namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;
use App\Services\ChartsCivilService;
use App\Services;
use App\Repositories;

class InformesFamiliarController extends Controller {

	
	private $chartsCivil;
	private $globalChart;
	private $_repoCatalogos;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
		//$this->chartsCivil = new ChartsCivilService();
		$this->globalChart = new Services\GlobalFamiliarChartsService();
		$this->_repoCatalogos = new Repositories\RepositorioCatalogos();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getInformeGlobal()
	{
		
		/*$juzgados = \App\Models\Juzgado :: where('tipo','<>','P')
				    ->get();*/
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaFamiliar();
		$anyos = \App\Models\EntradaFamiliar :: distinct()
				 ->selectRaw("CAST(año as CHAR(4)) as year")
				 ->orderBy("año","desc")
				 ->lists("year");
		
		$charts = array("General" => $this->globalChart->general->getTotal(),
						"Entradas" => $this->globalChart->entradas->getTotal(),
						"Salidas" => $this->globalChart->salidas->getTotal()
			      );
		$juicios = \App\Models\Juicio :: where('id_materia',3)->get();

		/*
		foreach ($juicios as $j) {
			$mat[] = $j->materia;
		}
		$materias = (new \Illuminate\Database\Eloquent\Collection($mat))->unique();	
		*/

		$rubros = $juicios->first()->rubroJuicios;
		return view('informes\detallefamiliar',['titulo' => 'Global'
										   , 'path' => '/informe/familiar/global/'
										   , 'label_juzgado' => 'Todos'
										   , 'label_anyo' => 'Todos'
										   //, 'materias' => $materias
										   , 'juicios' => $juicios
										   , 'rubros' => $rubros
										   , 'juzgados' => $juzgados
										   , 'anyos' => $anyos
										   , 'charts' => $charts]);
	}

	public function getInformeGlobalAnual($anyo)
	{
		/*$juzgados = \App\Models\Juzgado :: where('tipo','<>','P')
				    ->get();*/
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaFamiliar();
		$anyos = \App\Models\EntradaFamiliar :: distinct()
				 ->selectRaw("CAST(año as CHAR(4)) as year")
				 ->orderBy("año","desc")
				 //->get()
				 ->lists("year");
		$charts = array("General" => $this->globalChart->general->getAnual($anyo),
						"Entradas" => $this->globalChart->entradas->getAnual($anyo),
						"Salidas" => $this->globalChart->salidas->getAnual($anyo)
			);
		$juicios = \App\Models\Juicio :: where('id_materia',3)->get();

		/*
		foreach ($juicios as $j) {
			$mat[] = $j->materia;
		}
		$materias = (new \Illuminate\Database\Eloquent\Collection($mat))->unique();	
		*/

		$rubros = $juicios->first()->rubroJuicios;
		return view('informes\detallefamiliar',['titulo' => 'Global,'.$anyo
										   , 'path' => '/informe/familiar/global/'
										   , 'label_juzgado' => 'Todos'
										   , 'label_anyo' => $anyo
										   //, 'materias' => $materias
										   , 'juicios' => $juicios
										   , 'rubros' => $rubros
										   , 'juzgados' => $juzgados
										   , 'anyos' => $anyos
			                               , 'charts' => $charts]);
	}

	public function getInformeGlobalPorJuzgado($prefijo)
	{
		/*$juzgados = \App\Models\Juzgado :: where('tipo','<>','P')
				    ->get();*/
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaFamiliar();
		$juzgado = $juzgados->filter(function($item) use ($prefijo){
					return $item->prefijo == $prefijo;
			  })
		      ->first();

		$juicios = $juzgado->tipojuzgado->getJuicios();

		/*foreach ($juicios as $j) {
			$mat[] = $j->materia;
		}
		$materias = (new \Illuminate\Database\Eloquent\Collection($mat))->unique();
		*/

		$rubros = $juicios->first()->rubroJuicios;
		$anyos = \App\Models\EntradaFamiliar :: distinct()
				 ->selectRaw("CAST(año as CHAR(4)) as year")
				 ->where("id_juzgado","=", $juzgado->id_juzgado)
				 ->orderBy("año","desc")
				 ->lists("year");
		$charts = array("General" => $this->globalChart->general->getByJuzgadoTotal($juzgado),
						"Entradas" => $this->globalChart->entradas->getByJuzgadoTotal($juzgado),
						"Salidas" => $this->globalChart->salidas->getByJuzgadoTotal($juzgado)
			);
		return view('informes\detallefamiliar',['titulo' => $prefijo
										   , 'path' => '/informe/familiar/juzgado/'.$prefijo
										   , 'label_juzgado' => $prefijo
										   , 'label_anyo' => 'Todos'
										   //, 'materias' => $materias
										   , 'juicios' => $juicios
										   , 'rubros' => $rubros
										   , 'juzgados' => $juzgados
										   , 'anyos' => $anyos
			                               , 'charts' => $charts]);
	}

	public function getInformePorJuzgadoAnual($prefijo, $anyo)
	{
		/*$juzgados = \App\Models\Juzgado :: where('tipo','<>','P')
				    ->get();*/
		$juzgados = $this->_repoCatalogos->getJuzgadosRamaFamiliar();
		$juzgado = $juzgados->filter(function($item) use ($prefijo){
					return $item->prefijo == $prefijo;
			  })
		      ->first();

		$juicios = $juzgado->tipojuzgado->getJuicios();
		/*
		foreach ($juicios as $j) {
			$mat[] = $j->materia;
		}
		$materias = (new \Illuminate\Database\Eloquent\Collection($mat))->unique();	
		*/
		$rubros = $juicios->first()->rubroJuicios;

		$anyos = \App\Models\EntradaFamiliar :: distinct()
				 ->selectRaw("CAST(año as CHAR(4)) as year")
				 ->orderBy("año","desc")
				 //->get()
				 ->lists("year");
		$charts = array("General" => $this->globalChart->general->getByJuzgadoAnual($juzgado,$anyo),
						"Entradas" => $this->globalChart->entradas->getByJuzgadoAnual($juzgado,$anyo),
						"Salidas" => $this->globalChart->salidas->getByJuzgadoAnual($juzgado,$anyo)
			);
		return view('informes\detallefamiliar',['titulo' => $prefijo. ','.$anyo
										   , 'path' => '/informe/familiar/juzgado/'.$prefijo
										   , 'label_juzgado' => $prefijo
										   , 'label_anyo' => $anyo
										   //, 'materias' => $materias
										   , 'juicios' => $juicios
										   , 'rubros' => $rubros
										   , 'juzgados' => $juzgados
										   , 'anyos' => $anyos
			                               , 'charts' => $charts]);
	}
}