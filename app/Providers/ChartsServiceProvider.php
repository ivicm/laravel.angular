<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \App\Services\ChartService;

/**
* Register our pokemon service with Laravel
*/
class ChartsServiceProvider extends ServiceProvider 
{
    /**
    * Registers the service in the IoC Container
    * 
    */
    public function register()
    {
        // Binds 'chartsService' to the result of the closure
        $this->app->bind('chartsService', function($app)
        {
            return new ChartsService();
        });
    }
}