<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \App\Services\ChartService;

/**
* Register our pokemon service with Laravel
*/
class ChartsCivilServiceProvider extends ServiceProvider 
{
    /**
    * Registers the service in the IoC Container
    * 
    */
    public function register()
    {
        // Binds 'chartsService' to the result of the closure
        $this->app->bind('chartsCivilService', function($app)
        {
            return new ChartsCivilService();
        });
    }
}