<?php namespace App\Contracts;

interface IChartScopeFamiliar {
	public function getTotal();
	public function getAnual($anyo);
	public function getByJuzgadoTotal(\App\Models\Juzgado $juzgado);
	public function getByJuzgadoAnual(\App\Models\Juzgado $juzgado, $anyo);
}