<?php namespace App\Contracts;

use App\Models;
use App\Contracts;

abstract class AbsFamiliarChartService {

	public $general;
	public $entradas;
	public $salidas;
}