<?php namespace App\Contracts;

interface IRepositorioFamiliar {
	public function getReporteJuzgadoByJuicioAnual($id_juzgado, $id_juicio, $anyo);
	public function getReporteJuzgadoByJuicioDesgloseAnual($id_juzgado, $id_juicio, $anyo);
	public function getReporteJuzgadoByRubroAnual($id_juzgado, $id_rubroJuicio, $anyo);
	public function getReporteJuzgadoByRubroDesgloseAnual($id_juzgado, $id_rubroJuicio, $anyo);
}